#pragma once

#include "Menuses/Inventory/InventoryItem.h"
#include "InventoryItem_Usable.generated.h"

UCLASS()
class SURVIVE_API AInventoryItem_Usable : public AInventoryItem
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TSubclassOf<AActor> usableClass;

	UFUNCTION(BlueprintCallable, Category = "Item use")
		virtual void Use();
};
