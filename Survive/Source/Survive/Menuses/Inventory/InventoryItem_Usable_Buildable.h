// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Menuses/Inventory/InventoryItem_Usable.h"
#include "InventoryDB.h"
#include "InventoryItem_Usable_Buildable.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVE_API AInventoryItem_Usable_Buildable : public AInventoryItem_Usable
{
	GENERATED_BODY()

public:
	AInventoryItem_Usable_Buildable();

	void Use() override;
};
