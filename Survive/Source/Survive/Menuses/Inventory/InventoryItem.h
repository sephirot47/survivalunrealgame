#pragma once

#include "GameFramework/Actor.h"
#include "InventoryItem.generated.h"

class ASPlayer;

UENUM(BlueprintType)
enum class EInventoryItemCategory : uint8
{
	Buildings,
	Weapons,
	Crafting,
	Others
};

class AInventoryItem;

//Used to store the amount of one kind of ingredient to craft an item
USTRUCT(BlueprintType)
struct FSCraftIngredient
{
	GENERATED_BODY()

	//The class of crafting item of this ingredient type
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IngredientRow")
		TSubclassOf<AInventoryItem> ingredientClass;

	//Amount of the ingredient needed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IngredientRow")
		int32 amountNeeded;
};


UCLASS()
class SURVIVE_API AInventoryItem : public AActor
{
	GENERATED_BODY()

protected:
	ASPlayer *player;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UTexture2D *thumbnail;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		EInventoryItemCategory category; //Is it a Weapon, Buildable, Crafting or Other?

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 amount; //Amount of this item available

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool countable; //Can this item be counted (x1, x2, ..., x999) ? Or can't it (weapons for example) ? 

	//The ingredients needed to craft this item. If empty, this item can't be crafted :)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crafting")
		TArray<FSCraftIngredient> craftIngredientsNeeded;

	AInventoryItem();
	~AInventoryItem();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
};
