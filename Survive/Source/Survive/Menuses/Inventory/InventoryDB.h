#pragma once

#include "GameFramework/Actor.h"
#include "InventoryItem.h"
#include "InventoryItem_Ammo.h"
#include "../../InGame/Weapons/SFireWeapon.h"
#include "InventoryDB.generated.h"

UCLASS()
class SURVIVE_API AInventoryDB : public AActor
{
	GENERATED_BODY()

public:	

	static AInventoryDB *inventory;

	//The array of weapon items
	UPROPERTY(BlueprintReadOnly, Category = "Inventory Items")
		TArray<AInventoryItem*> items;

	//The array of craftable AInventoryItem classes
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory Items")
		TArray< TSubclassOf<AInventoryItem> > craftableItemsClasses;

	AInventoryDB();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	//Gets the totalAmmo, and the currentAmmo (ammo that was loaded in the weapon last time the weapon was saved
	void GetAmmoForThisTypeOfWeapon(TSubclassOf<ASFireWeapon> FireWeaponClass, int &totalAmmo, int &currentAmmo);

	//Saves the totalAmmo, and the currentAmmo for a specified type of weapon
	//This should be called when switching the weapon(current weapon 'goes into inventory'), for example
	void SaveAmmoForThisTypeOfWeapon(TSubclassOf<ASFireWeapon> FireWeaponClass, int totalAmmo, int currentAmmo);

	//Returns how many items of the class passed as parameter, taking into account the ingredients
	//available in the inventory
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int32 GetCraftTimes(TSubclassOf<AInventoryItem> itemClass);

	//Tries to craft(adds to inventory) the amount of items specified, of the specified class, and remove all the necessary
	//ingredients used
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void CraftItem(TSubclassOf<AInventoryItem> itemClass, int32 craftAmount = 1);

	//Returns the amount of this type of item in the inventory. 0 if there's none
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int32 GetAmount(TSubclassOf<AInventoryItem> itemClass);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void AddItem(TSubclassOf<AInventoryItem> itemClass, int32 amount = 1);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		AInventoryItem* GetItem(TSubclassOf<AActor> itemClass);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void RemoveItem(TSubclassOf<AActor> itemClass, int32 amount = 1);
};
