#pragma once

#include "Menuses/Inventory/InventoryItem_Usable.h"
#include "InventoryItem_Usable_Weapon.generated.h"

UCLASS()
class SURVIVE_API AInventoryItem_Usable_Weapon : public AInventoryItem_Usable
{
	GENERATED_BODY()

public:

	AInventoryItem_Usable_Weapon();
	void Use() override;
};
