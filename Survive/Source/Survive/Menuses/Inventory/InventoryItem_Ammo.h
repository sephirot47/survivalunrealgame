#pragma once

#include "Menuses/Inventory/InventoryItem.h"
#include "InventoryItem_Ammo.generated.h"

UCLASS()
class SURVIVE_API AInventoryItem_Ammo : public AInventoryItem
{
	GENERATED_BODY()

public:

	AInventoryItem_Ammo();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 currentAmmo; //The ammo that was loaded on the weapon the last time it was saved in the inventory.

	//The totalAmmo can be retrieved using the amount variable of the item itself. 
	//The totalAmmo DOESNT include the currentAmmo loaded in the weapon.

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TSubclassOf<AActor> weaponForThisAmmoClass;
};
