// Fill out your copyright notice in the Description page of Project Settings.

#include "Survive.h"
#include "InventoryItem_Usable_Weapon.h"

AInventoryItem_Usable_Weapon::AInventoryItem_Usable_Weapon()
{
	category = EInventoryItemCategory::Weapons;
}


void AInventoryItem_Usable_Weapon::Use()
{
	player->GetWeaponManager()->OnWeaponsMenuItemSelected(this->usableClass);
}


