#include "Survive.h"
#include "InventoryDB.h"

AInventoryDB *AInventoryDB::inventory = nullptr;

AInventoryDB::AInventoryDB()
{
	PrimaryActorTick.bCanEverTick = true;
	items = TArray<AInventoryItem*>();
	AInventoryDB::inventory = this;
}

void AInventoryDB::BeginPlay()
{
	Super::BeginPlay();

	//Fill the inventory with the item actors you find in the map
	for (TActorIterator<AActor> actor(GetWorld()); actor; ++actor)
	{
		AInventoryItem *item = Cast<AInventoryItem>(*actor);
		if (item) items.Add(item);
	}
}

void AInventoryDB::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );
}

void AInventoryDB::GetAmmoForThisTypeOfWeapon(TSubclassOf<ASFireWeapon> FireWeaponClass, int &totalAmmo, int &currentAmmo)
{
	for (int i = 0; i < items.Num(); ++i)
	{
		AInventoryItem *item = items[i]; if (!item) continue;
		AInventoryItem_Ammo *ammoItem = Cast<AInventoryItem_Ammo>(item);
		if (ammoItem)
		{
			if (ammoItem->weaponForThisAmmoClass == FireWeaponClass) //It's an ammo for the weapon we are looking the ammo amount for :)
			{
				totalAmmo = ammoItem->amount;
				currentAmmo = ammoItem->currentAmmo;
				return;
			}
		}
	}
	totalAmmo = 0; currentAmmo = 0;
}

void AInventoryDB::SaveAmmoForThisTypeOfWeapon(TSubclassOf<ASFireWeapon> FireWeaponClass, int totalAmmo, int currentAmmo)
{
	for (AInventoryItem *item : items)
	{
		AInventoryItem_Ammo *ammoItem = Cast<AInventoryItem_Ammo>(item);
		if (ammoItem)
		{
			if (ammoItem->weaponForThisAmmoClass == FireWeaponClass) //It's an ammo for the weapon we are looking the ammo amount for :)
			{
				ammoItem->amount = totalAmmo;
				ammoItem->currentAmmo = currentAmmo;
			}
		}
	}
}

int32 AInventoryDB::GetCraftTimes(TSubclassOf<AInventoryItem> itemClass)
{
	int32 minDivision = -1;
	AInventoryItem *item = Cast<AInventoryItem>(GetWorld()->SpawnActor(itemClass));
	for (FSCraftIngredient craftIngredientNeeded : item->craftIngredientsNeeded)
	{
		int32 availableAmount = this->GetAmount(craftIngredientNeeded.ingredientClass);
		int32 neededAmount = craftIngredientNeeded.amountNeeded;
		int32 division = availableAmount / neededAmount;
		if (division < minDivision || minDivision < 0) minDivision = division;
	}
	item->Destroy(); //Destroy the temporary item created to get the needed ingredients structs :)
	return minDivision < 0 ? 0 : minDivision;
}

void AInventoryDB::CraftItem(TSubclassOf<AInventoryItem> itemClass, int32 craftAmount)
{
	int32 possibleCraftTimes = GetCraftTimes(itemClass);
	if (possibleCraftTimes >= craftAmount)
	{
		AInventoryItem *item = Cast<AInventoryItem>(GetWorld()->SpawnActor(itemClass));
		for (FSCraftIngredient craftIngredientNeeded : item->craftIngredientsNeeded)
		{
			//Remove every ingredients as many times as used
			int32 neededAmount = craftIngredientNeeded.amountNeeded;
			RemoveItem(craftIngredientNeeded.ingredientClass, neededAmount * craftAmount); 
		}
		item->Destroy(); //Destroy the temporary item created to get the needed ingredients structs :)
	}

	AddItem(itemClass, craftAmount); //CRAFT IT :D
}

int32 AInventoryDB::GetAmount(TSubclassOf<AInventoryItem> itemClass)
{
	AInventoryItem *item = GetItem(itemClass);
	if (!item) return 0;
	else return item->amount;
}

void AInventoryDB::AddItem(TSubclassOf<AInventoryItem> itemClass, int32 amount)
{
	AInventoryItem *item = GetItem(itemClass);
	if (item) item->amount += amount; //If there was a type of this item in the inventory, just update the item amount
	else
	{
		AInventoryItem *item = Cast<AInventoryItem>(GetWorld()->SpawnActor(itemClass));
		if (item)
		{
			items.Add(item); 
			item->amount = amount;
		}
	}
}

AInventoryItem* AInventoryDB::GetItem(TSubclassOf<AActor> itemClass)
{
	if (!itemClass->IsChildOf(AInventoryItem::StaticClass())) return nullptr;

	for (AInventoryItem *item : items)
	{
		if (item->GetClass() == itemClass) return item;
	}
	return nullptr;
}

void AInventoryDB::RemoveItem(TSubclassOf<AActor> itemClass, int32 amount)
{
	if (!itemClass->IsChildOf(AInventoryItem::StaticClass())) return;

	AInventoryItem *item = GetItem(itemClass);
	if (item)
	{
		item->amount -= amount; //Decrease the item amount
		if (item->amount <= 0) items.Remove(item); //If the amount is below 0, erase it from the items array
	}
}

