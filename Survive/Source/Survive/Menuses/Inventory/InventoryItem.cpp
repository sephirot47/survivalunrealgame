#include "Survive.h"
#include "InventoryItem.h"
#include "InGame/Characters/Player/SPlayer.h"

AInventoryItem::AInventoryItem()
{
	name = "";
	description = "";
	thumbnail = nullptr;
	category = EInventoryItemCategory::Others;
	amount = 1;
	countable = true;

	PrimaryActorTick.bCanEverTick = false;
}

AInventoryItem::~AInventoryItem()
{

}

void AInventoryItem::BeginPlay()
{
	Super::BeginPlay();
	player = Cast<ASPlayer>(GetWorld()->GetFirstPlayerController()->GetCharacter());
}

void AInventoryItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}



