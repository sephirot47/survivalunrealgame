// Fill out your copyright notice in the Description page of Project Settings.

#include "Survive.h"
#include "InventoryItem_Usable_Buildable.h"


AInventoryItem_Usable_Buildable::AInventoryItem_Usable_Buildable()
{
	category = EInventoryItemCategory::Buildings;
}

void AInventoryItem_Usable_Buildable::Use()
{
	player->GetBuildManager()->OnBuildingsMenuItemSelected(this->usableClass);
	
	//Remove the item from the inventory
	AInventoryDB *inventoryDB = nullptr;
	for(TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		inventoryDB = Cast<AInventoryDB>(*ActorItr);
		if (inventoryDB) inventoryDB->RemoveItem(this->GetClass(), 1);
	}
}