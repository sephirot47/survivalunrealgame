#include "Survive.h"
#include "SWaveManager.h"

ASWaveManager::ASWaveManager()
{
	PrimaryActorTick.bCanEverTick = true;

	currentWaveIndex = 0;
	timeSinceCurrentWaveStarted = timeSinceCurrentWaveFinished = 0.0f;
	currentWaveState = EWaveState::InWave;
}

void ASWaveManager::BeginPlay()
{
	Super::BeginPlay();
	OnCurrentWaveJustStarted();
}

void ASWaveManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (currentWaveState == EWaveState::InWave) 
	{
		timeSinceCurrentWaveStarted += DeltaTime;
		timeSinceCurrentWaveFinished = .0f;
		if (timeSinceCurrentWaveStarted >= currentWave.waveDuration) OnCurrentWaveJustFinished();
		else
		{	//Wave still running
			for (int i = 0; i < currentWave.enemyGroupWaves.Num(); ++i)
			{
				CheckForEnemyGroupWaveSpawning( currentWave.enemyGroupWaves[i] );
			}
		}

	}
	else if (currentWaveState == EWaveState::BetweenWaves) 
	{
		timeSinceCurrentWaveStarted = .0f;
		timeSinceCurrentWaveFinished += DeltaTime;
		if (timeSinceCurrentWaveFinished >= currentWave.delayAfterWave)
		{
			if (currentWaveIndex < waves.Num()) OnCurrentWaveJustStarted();
		}
	}
}

void ASWaveManager::OnCurrentWaveJustStarted()
{
	timeSinceCurrentWaveStarted = 0.0f;
	timeSinceCurrentWaveFinished = 0.0f;
	currentWave = waves[currentWaveIndex];
	currentWaveState = EWaveState::InWave;
}

void ASWaveManager::OnCurrentWaveJustFinished()
{
	timeSinceCurrentWaveStarted = 0.0f;
	timeSinceCurrentWaveFinished = 0.0f;
	++currentWaveIndex;
	currentWaveState = EWaveState::BetweenWaves;
}

void ASWaveManager::CheckForEnemyGroupWaveSpawning(FSEnemyGroupWave &enemyGroupWave)
{
	float enemyGroupWaveSpawnBeginTime = currentWave.waveDuration * enemyGroupWave.spawnBeginTimePercent;
	float enemyGroupWaveDuration = currentWave.waveDuration * enemyGroupWave.groupWaveDurationPercent;
	float enemyGroupWaveSpawnEndTime = enemyGroupWaveSpawnBeginTime + enemyGroupWaveDuration;

	if (timeSinceCurrentWaveStarted >= enemyGroupWaveSpawnBeginTime &&
		timeSinceCurrentWaveStarted <= enemyGroupWaveSpawnEndTime) //We are in the time of this wave (between begin and end)
	{
		if (enemyGroupWave.spawnedEnemies < enemyGroupWave.amountEnemies)
		{
			//wavePercentTime: if it's 0.0 it means it's the begin of the enemygroupwave, 1.0 means the end, 0.5 half, etc.
			float wavePercentTime = (timeSinceCurrentWaveStarted - enemyGroupWaveSpawnBeginTime) / enemyGroupWaveDuration;
			float enemyDelay = enemyGroupWaveDuration / (FMath::Max(1, enemyGroupWave.amountEnemies - 1));
			float nextEnemySpawnTime = enemyDelay * enemyGroupWave.spawnedEnemies;
			if (timeSinceCurrentWaveStarted - enemyGroupWaveSpawnBeginTime >= nextEnemySpawnTime)
			{
				SpawnEnemyFromEnemyGroupWave(enemyGroupWave);
			}
		}

	}
	else enemyGroupWave.spawnedEnemies = 0;
}

void ASWaveManager::SpawnEnemyFromEnemyGroupWave(FSEnemyGroupWave &enemyGroupWave)
{
	FVector enemyLocation = GetRandomSpawnLocation();
	AActor *enemyActor = GetWorld()->SpawnActor(enemyGroupWave.EnemyClass, &enemyLocation);
	ASEnemy *enemy = Cast<ASEnemy>(enemyActor);
	if (enemy)
	{
		enemy->SpawnDefaultController();
		enemyGroupWave.spawnedEnemies++;
	}
}


FVector ASWaveManager::GetRandomSpawnLocation()
{
	//Get the random spawnVolume
	if (spawnVolumes.Num() <= 0) return FVector::ZeroVector;
	int randIndex = FMath::RandRange(0, spawnVolumes.Num() - 1);
	AActor *spawnVolume = spawnVolumes[randIndex];

	FVector origin, extents;
	spawnVolume->GetActorBounds(true, origin, extents);

	float minX, minY, minZ, maxX, maxY, maxZ;
	minX = origin.X - extents.X / 2; maxX = origin.X + extents.X / 2;
	minY = origin.Y - extents.Y / 2; maxY = origin.Y + extents.Y / 2;
	minZ = origin.Z - extents.Z / 2; maxZ = origin.Z + extents.Z / 2;

	return FVector(FMath::FRandRange(minX, maxX), FMath::FRandRange(minY, maxY), FMath::FRandRange(minZ, maxZ));
}

EWaveState ASWaveManager::GetCurrentWaveState()
{
	return currentWaveState;
}

float ASWaveManager::GetCurrentWaveTimePercent() 
{
	return timeSinceCurrentWaveStarted / currentWave.waveDuration;
}

FString ASWaveManager::GetCurrentWaveTimeText()
{
	return GetMinutesString(currentWave.waveDuration - timeSinceCurrentWaveStarted);
}

float ASWaveManager::GetCurrentDelayAfterWaveTimePercent()
{
	return timeSinceCurrentWaveFinished / currentWave.delayAfterWave;
}

FString ASWaveManager::GetCurrentDelayAfterWaveTimeText()
{
	return GetMinutesString(currentWave.delayAfterWave - timeSinceCurrentWaveFinished);
}

FString ASWaveManager::GetMinutesString(float timePassed)
{
	FString minutesString = "";
	int minutes = timePassed / 60.0f;
	int seconds = FMath::CeilToInt(timePassed) % 60;
	return FString::Printf(TEXT("%d:%d"), minutes, seconds);
}