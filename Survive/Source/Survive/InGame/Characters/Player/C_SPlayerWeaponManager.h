#pragma once

#include "Components/ActorComponent.h"
#include "../../Weapons/SWeapon.h"
#include "../../../SUtils.h"
#include "../../Weapons/SFireWeapon.h"
#include "C_SPlayerWeaponManager.generated.h"

class ASPlayer;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_SPlayerWeaponManager : public UActorComponent
{
	GENERATED_BODY()

private:

	ASWeapon *currentWeapon;
	ASPlayer *player;

	TSubclassOf<ASWeapon> LastEquippedWeaponClass;

	void OnDrawWeaponInput();

	void EquipWeapon(ASWeapon *weapon);
	void UnEquipCurrentWeapon();

public:	

	//This is the socket where the weapon will attach
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Weapon")
		FName weaponSocketName;

	UC_SPlayerWeaponManager();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	void OnBeginMovingBuildable();
	void OnFinishedMovingBuildable();

	UFUNCTION(BlueprintCallable, Category = "Player Weapon")
		bool IsShooting();

	UFUNCTION(BlueprintCallable, Category = "Player Weapon")
		bool IsReloading();

	UFUNCTION(BlueprintCallable, Category = "Player Weapon")
		void OnWeaponsMenuItemSelected(TSubclassOf<AActor> WeaponClass);

	UFUNCTION(BlueprintCallable, Category = "Player Weapon")
		ASWeapon* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable, Category = "Player Weapon")
		bool HasWeaponEquipped();


	void OnItemLooted();
};
