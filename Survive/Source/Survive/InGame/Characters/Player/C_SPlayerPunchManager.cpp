#include "Survive.h"
#include "C_SPlayerPunchManager.h"

UC_SPlayerPunchManager::UC_SPlayerPunchManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	usingPunches = false;
}

void UC_SPlayerPunchManager::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<ASPlayer>(GetOwner());
	weaponManager = player->GetWeaponManager();
	buildManager = player->GetBuildManager();

	TArray<USphereComponent*> sphereColliders; player->GetComponents<USphereComponent>(sphereColliders);
	for (USphereComponent *sphereCollider : sphereColliders)
	{
		static int i = 0;
		GEngine->AddOnScreenDebugMessage(1244 + (++i), 3.0f, FColor::Cyan, sphereCollider->GetName());
		sphereCollider->OnComponentBeginOverlap.AddDynamic(this, &UC_SPlayerPunchManager::OnPunchesOverlapSomething);
	}	

	player->InputComponent->BindAction("Weapon Use", IE_Pressed, this, &UC_SPlayerPunchManager::OnInputPunchDown);
	player->InputComponent->BindAction("Weapon Use", IE_Released, this, &UC_SPlayerPunchManager::OnInputPunchUp);
}

void UC_SPlayerPunchManager::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	usingPunches = usingPunches && CanUsePunches();
}

void UC_SPlayerPunchManager::OnInputPunchDown()
{
	punchInputDown = true;
	if (CanUsePunches()) usingPunches = true;
}

void UC_SPlayerPunchManager::OnInputPunchUp()
{
	punchInputDown = false;
}

bool UC_SPlayerPunchManager::IsPunchInputDown()
{
	return punchInputDown;
}

void UC_SPlayerPunchManager::OnPunchMontageEnds()
{
	if (!punchInputDown) usingPunches = false;
}

bool UC_SPlayerPunchManager::CanUsePunches()
{
	return weaponManager && !weaponManager->HasWeaponEquipped() && 
		   buildManager && buildManager->GetCurrentBuildingState() != EPlayerBuildingState::MovingBuildable;
}

void UC_SPlayerPunchManager::OnPunchesOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (CanUsePunches() && usingPunches && OtherActor != player)
	{
		IDamageReceiver *damageReceiver = Cast<IDamageReceiver>(OtherActor);
		if (damageReceiver)
		{
			damageReceiver->ReceiveDamage(OtherActor, this->punchDamage);
		}
	}
}