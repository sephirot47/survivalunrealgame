#include "Survive.h"
#include "SPlayer.h"
#include "C_SPlayerBuildManager.h"


UC_SPlayerBuildManager::UC_SPlayerBuildManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	currentState = EPlayerBuildingState::PointingBuildable;
	targetPoint = FVector::ZeroVector;
	rotateInputDown = false;
	justStartedMovingBuildable = false;
	buildableSelectionRange = 500.0f;
	
	targetBuildable = nullptr;
}


void UC_SPlayerBuildManager::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<ASPlayer>(GetOwner());
	player->InputComponent->BindAction("Move Buildable", IE_Pressed, this, &UC_SPlayerBuildManager::OnInputMoveBuildable);
	player->InputComponent->BindAction("Put Buildable", IE_Pressed, this, &UC_SPlayerBuildManager::OnInputPutBuildable);
	player->InputComponent->BindAction("Rotate Buildable", IE_Pressed, this, &UC_SPlayerBuildManager::OnInputRotateBuildableDown);
	player->InputComponent->BindAction("Rotate Buildable", IE_Released, this, &UC_SPlayerBuildManager::OnInputRotateBuildableUp);
	player->InputComponent->BindAction("Remove Buildable", IE_Pressed, this, &UC_SPlayerBuildManager::OnInputRemoveBuildable);
}

void UC_SPlayerBuildManager::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	lastTargetBuildable = targetBuildable;
	FillTargetBuildableInfo(); //Fill targetBuildable and targetPoint variables

	//The buildable we were pointing isn't pointed anymore. Has to go from OnPointingOver to OnBuilt 
	if (lastTargetBuildable && lastTargetBuildable != targetBuildable) lastTargetBuildable->OnPointedOut();

	if ( targetBuildable && BuildableInRange(targetBuildable) ) //While pointing over a targetBuildable...
	{
		if (targetBuildable->GetCurrentState() == EBuildableState::Built) //If it's the first frame we point to a buildable...
			targetBuildable->OnBeingPointedOver();
	}

	if (currentState == EPlayerBuildingState::MovingBuildable && targetBuildable) //We are moving a buildable
	{
		targetBuildable->SetActorLocation(targetPoint); //Move the buildable
		if (!BuildableInRange(targetPoint)) targetBuildable->OnOutOfPlayerBuildRange();
		else targetBuildable->OnInsideOfPlayerBuildRange();
		player->GetMovementManager()->currentBuildableWeight = targetBuildable->weight; //Update the player buildable weight
	}
	else if (currentState == EPlayerBuildingState::PointingBuildable)
	{
		if (targetBuildable && !BuildableInRange(targetBuildable))  targetBuildable->OnPointedOut();
		player->GetMovementManager()->currentBuildableWeight = 0.0f; //Update the player buildable weight
	}

	if (rotateInputDown) RotateTargetBuildable();
}

void UC_SPlayerBuildManager::OnInputMoveBuildable()
{
	if (!targetBuildable || !BuildableInRange(targetBuildable)) return;

	//Start moving the buildable
	if (currentState == EPlayerBuildingState::PointingBuildable && 
		targetBuildable->GetCurrentState() == EBuildableState::BeingPointedOver)  //The buildable cant be being built
	{
		if (targetBuildable)
		{
			currentState = EPlayerBuildingState::MovingBuildable;
			targetBuildable->OnBeingMoved();
			justStartedMovingBuildable = true;
			player->GetWeaponManager()->OnBeginMovingBuildable();
		}
	}
}

//This function adds the left click input for when you want to put the buildable, but not for moving it
void UC_SPlayerBuildManager::OnInputPutBuildable()
{
	//Si tiene targetBuildable y esta apuntando a algun sitio donde pueda ponerlo...
	if (currentState == EPlayerBuildingState::MovingBuildable && !justStartedMovingBuildable &&
		targetBuildable && targetPoint != FVector::ZeroVector && targetBuildable->CanBeBuilt())
	{
		if (BuildableInRange(targetBuildable))
		{
			currentState = EPlayerBuildingState::PointingBuildable;
			targetBuildable->OnBeingBuilt();  //Empieza a construirlo
			rotateInputDown = false;
			player->GetWeaponManager()->OnFinishedMovingBuildable();
		}
	}
	justStartedMovingBuildable = false;
}

void UC_SPlayerBuildManager::RotateTargetBuildable()
{
	if (targetBuildable && rotateInputDown)
	{
		FRotator rot;
		float rotSpeed = targetBuildable->GetBuildingRotationSpeed();
		FVector euler(0.0f, 0.0f, rotSpeed);
		rot.MakeFromEuler(euler);
		targetBuildable->AddActorWorldRotation(rot);
	}
}

void UC_SPlayerBuildManager::OnInputRemoveBuildable() 
{
	if (targetBuildable && targetPoint != FVector::ZeroVector && currentState == EPlayerBuildingState::MovingBuildable &&
		targetBuildable->GetHealthPercent() >= 1.0f)
	{
		PutCurrentBuildableBackInInventory();
		player->GetWeaponManager()->OnFinishedMovingBuildable();
	}
}

//Returns the current buildable back to the inventory
void UC_SPlayerBuildManager::PutCurrentBuildableBackInInventory()
{
	if (targetBuildable && targetBuildable->GetCurrentState() == EBuildableState::BeingMoved)
	{
		for (TActorIterator<AActor> actor(GetWorld()); actor; ++actor)
		{
			AInventoryDB *inventoryDB = Cast<AInventoryDB>(*actor);
			if (inventoryDB)
			{
				if (targetBuildable->CorrespondingInventoryItemClass)
				{
					inventoryDB->AddItem(targetBuildable->CorrespondingInventoryItemClass, 1);
					break;
				}
			}
		}

		currentState = EPlayerBuildingState::PointingBuildable;
		targetBuildable->OnDestroy();
		targetBuildable = nullptr;
		player->GetWeaponManager()->OnFinishedMovingBuildable();
	}
}

void UC_SPlayerBuildManager::OnBuildingsMenuItemSelected(TSubclassOf<AActor> BuildableClass)
{
	if (!BuildableClass->IsChildOf(ASBuildable::StaticClass())) return;
	
	AActor *actor = GetWorld()->SpawnActor(BuildableClass);
	ASBuildable *buildable = Cast<ASBuildable>(actor);
	
	//If the player was pointing any building, build it (quit the onpointing)
	//(it will never be built in a wrong place because you can only enter the building menu if the player
	// wasnt moving any buildable)
	if (targetBuildable) targetBuildable->OnBeingBuilt();
	currentState = EPlayerBuildingState::MovingBuildable;

	targetBuildable = buildable;
	targetBuildable->OnBeingMoved();
	player->GetWeaponManager()->OnBeginMovingBuildable();
}

EPlayerBuildingState UC_SPlayerBuildManager::GetCurrentBuildingState() { return currentState; }
bool UC_SPlayerBuildManager::BuildableInRange(const FVector &buildableLocation){ return FVector::Dist(player->GetActorLocation(), buildableLocation) <= buildableSelectionRange; }
bool UC_SPlayerBuildManager::BuildableInRange(ASBuildable *buildable) { return BuildableInRange(buildable->GetActorLocation());}
void UC_SPlayerBuildManager::OnInputRotateBuildableDown()  { if (currentState == EPlayerBuildingState::MovingBuildable) rotateInputDown = true; }
void UC_SPlayerBuildManager::OnInputRotateBuildableUp()  { rotateInputDown = false; }

void UC_SPlayerBuildManager::FillTargetBuildableInfo()
{
	TArray<AActor*> ignoredActors;
	ignoredActors.Add(GetOwner()); //Ignore myself (the player)

	APlayerCameraManager *camManager = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	FVector traceStart = camManager->GetCameraLocation();
	FVector traceEnd = traceStart + camManager->GetCameraRotation().Vector() * 99999.9f;
	//DrawDebugLine(GetWorld(), traceStart, traceEnd, FColor::Green, true, 1.0f);

	ECollisionChannel channel;
	if (currentState == EPlayerBuildingState::PointingBuildable) channel = PlayerPointingTraceChannel;
	else channel = PlayerMovingTraceChannel;

	FHitResult hitResult;
	if (SUtils::Trace(ignoredActors, traceStart, traceEnd, hitResult, channel))
	{
		//Hitting something!
		targetPoint = hitResult.Location;

		//Only when Pointing. If we are Moving, we must not override targetBuildable 
		if (currentState == EPlayerBuildingState::PointingBuildable)
		{
			ASBuildable *buildable = Cast<ASBuildable>(hitResult.GetActor());
			targetBuildable = buildable; //If the hit object is not a buildable, then the cast fails, hence buildable = nullptr :)
		}
		if(targetBuildable) targetBuildable->SetHidden(false);
	}
	else //Don't hit anything
	{
		targetPoint = FVector::ZeroVector;

		if (currentState == EPlayerBuildingState::PointingBuildable)  targetBuildable = nullptr;
		if (currentState == EPlayerBuildingState::MovingBuildable && targetBuildable) targetBuildable->SetHidden(true);
	}
}
