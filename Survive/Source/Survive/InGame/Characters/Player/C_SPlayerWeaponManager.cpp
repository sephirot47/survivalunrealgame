#include "Survive.h"
#include "C_SPlayerWeaponManager.h"
#include "SPlayer.h"

UC_SPlayerWeaponManager::UC_SPlayerWeaponManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	LastEquippedWeaponClass = nullptr;
}


void UC_SPlayerWeaponManager::BeginPlay()
{
	Super::BeginPlay();
	player = Cast<ASPlayer>(GetOwner());

	player->InputComponent->BindAction("Draw Weapon", IE_Pressed, this, &UC_SPlayerWeaponManager::OnDrawWeaponInput);
}


void UC_SPlayerWeaponManager::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

void UC_SPlayerWeaponManager::EquipWeapon(ASWeapon *weapon)
{
	currentWeapon = weapon;
	LastEquippedWeaponClass = weapon->GetClass();
	USkeletalMeshComponent *playerMesh = Cast<USkeletalMeshComponent>(player->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	weapon->AttachRootComponentTo(playerMesh, weaponSocketName, EAttachLocation::SnapToTarget);
	player->MoveIgnoreActorAdd(currentWeapon);
	player->GetMovementManager()->currentWeaponWeight = currentWeapon->GetWeight(); //Update the player weapon weight
}

void UC_SPlayerWeaponManager::UnEquipCurrentWeapon()
{
	if (currentWeapon)
	{
		currentWeapon->Destroy();
		currentWeapon = nullptr;
	}
	player->GetMovementManager()->currentWeaponWeight = 0.0f; //Update the player weapon weight
}

void UC_SPlayerWeaponManager::OnWeaponsMenuItemSelected(TSubclassOf<AActor> WeaponClass)
{
	player->GetBuildManager()->PutCurrentBuildableBackInInventory();
	UnEquipCurrentWeapon(); //Unequip if another weapon was equiped

	if (WeaponClass)
	{
		if (!WeaponClass->IsChildOf(ASWeapon::StaticClass())) return;

		AActor *actor = GetWorld()->SpawnActor(WeaponClass);
		if (actor)
		{
			ASWeapon *weapon = Cast<ASWeapon>(actor);
			if (weapon) EquipWeapon(weapon);
		}
	}
}

ASWeapon* UC_SPlayerWeaponManager::GetCurrentWeapon()
{
	return currentWeapon;
}

bool UC_SPlayerWeaponManager::HasWeaponEquipped()
{
	return currentWeapon != nullptr;
}

void UC_SPlayerWeaponManager::OnItemLooted()
{
	if (currentWeapon)
	{
		ASFireWeapon *fireWeapon = Cast<ASFireWeapon>(currentWeapon);
		if (fireWeapon)
		{
			fireWeapon->UpdateLocalAmmoFromInventory();
		}
	}
}

void UC_SPlayerWeaponManager::OnDrawWeaponInput()
{
	if (currentWeapon) UnEquipCurrentWeapon();
	else if (LastEquippedWeaponClass)
	{
		AActor *actor = GetWorld()->SpawnActor(LastEquippedWeaponClass);
		if (actor)
		{
			ASWeapon *weapon = Cast<ASWeapon>(actor);
			if (weapon) EquipWeapon(weapon);
		}
	}
}

void UC_SPlayerWeaponManager::OnBeginMovingBuildable()
{
	if(currentWeapon) LastEquippedWeaponClass = currentWeapon->GetClass();
	else LastEquippedWeaponClass = nullptr;

	UnEquipCurrentWeapon();
}

void UC_SPlayerWeaponManager::OnFinishedMovingBuildable()
{
	if (LastEquippedWeaponClass)
	{
		AActor *actor = GetWorld()->SpawnActor(LastEquippedWeaponClass);
		ASWeapon *weapon = Cast<ASWeapon>(actor);
		EquipWeapon(weapon);
		LastEquippedWeaponClass = nullptr;
	}
}

bool UC_SPlayerWeaponManager::IsReloading()
{
	ASFireWeapon *fireWeapon = Cast<ASFireWeapon>(currentWeapon);
	if (fireWeapon) return fireWeapon->Reloading();
	return false;
}

bool UC_SPlayerWeaponManager::IsShooting()
{
	return currentWeapon && currentWeapon->BeingUsed() && currentWeapon->CanBeUsed();
}