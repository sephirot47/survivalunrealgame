#include "Survive.h"
#include "SPlayer.h"
#include "C_SPlayerRecoilManager.h"

UC_SPlayerRecoilManager::UC_SPlayerRecoilManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	currentAccumulatedRecoil = FVector2D(0.0f, 0.0f);
	RecoilRecoveryRate = 0.97f;
}

void UC_SPlayerRecoilManager::BeginPlay()
{
	Super::BeginPlay();	
	player = Cast<ASPlayer>(GetOwner());
}

void UC_SPlayerRecoilManager::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	
	//Recover recoil
	FVector2D lastAccumulatedRecoil = currentAccumulatedRecoil;
	currentAccumulatedRecoil *= RecoilRecoveryRate;

	player->AddControllerYawInput(currentAccumulatedRecoil.X - lastAccumulatedRecoil.X);
	player->AddControllerPitchInput(currentAccumulatedRecoil.Y - lastAccumulatedRecoil.Y);
	//
}

void UC_SPlayerRecoilManager::OnShoot(const FVector2D &recoilForce, const FVector2D &recoilForceLimit)
{
	if (currentAccumulatedRecoil + recoilForce < recoilForceLimit)
	{
		currentAccumulatedRecoil += recoilForce;

		player->AddControllerYawInput(recoilForce.X);
		player->AddControllerPitchInput(recoilForce.Y);
	}
}

