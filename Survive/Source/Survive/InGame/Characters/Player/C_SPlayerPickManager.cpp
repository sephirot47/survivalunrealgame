#include "Survive.h"
#include "C_SPlayerPickManager.h"
#include "SPlayer.h"

UC_SPlayerPickManager::UC_SPlayerPickManager()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	pickRange = 300.0f;
	targetPickable = nullptr;
}


void UC_SPlayerPickManager::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<ASPlayer>(GetOwner());
	player->InputComponent->BindAction("Pick", IE_Pressed, this, &UC_SPlayerPickManager::OnPickInput);
}

void UC_SPlayerPickManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AActor *actorInFrontOfPlayer = player->GetActorInFrontOfPlayer(pickRange);
	ASPickableLoot *pickableInFrontOfPlayer = Cast<ASPickableLoot>(actorInFrontOfPlayer);
	if (pickableInFrontOfPlayer)
	{
		pickableInFrontOfPlayer->OnTargetted();
		if (targetPickable && targetPickable != pickableInFrontOfPlayer) targetPickable->OnUnTargetted();
		targetPickable = pickableInFrontOfPlayer;
	}
	else
	{
		if (targetPickable) targetPickable->OnUnTargetted();
		targetPickable = nullptr;
	}
}

void UC_SPlayerPickManager::OnPickInput()
{
	if (targetPickable) targetPickable->OnPicked();
}