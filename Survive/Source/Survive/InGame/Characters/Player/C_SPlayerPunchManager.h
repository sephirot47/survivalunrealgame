#pragma once

#include "Components/ActorComponent.h"
#include "SPlayer.h"
#include "C_SPlayerBuildManager.h"
#include "C_SPlayerWeaponManager.h"
#include "C_SPlayerPunchManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_SPlayerPunchManager : public UActorComponent
{
	GENERATED_BODY()

private:
	
	UPROPERTY(EditAnywhere, Category = "Punches") float punchDamage;
	ASPlayer *player;
	UC_SPlayerBuildManager *buildManager;
	UC_SPlayerWeaponManager *weaponManager;

	bool usingPunches;
	bool punchInputDown;

public:	
	UC_SPlayerPunchManager();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Punches")
		bool IsPunchInputDown();
	
	bool CanUsePunches();

	void OnInputPunchDown();
	void OnInputPunchUp();

	UFUNCTION(BlueprintCallable, Category = "Punches")
		void OnPunchMontageEnds();

	UFUNCTION()
		void OnPunchesOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
};
