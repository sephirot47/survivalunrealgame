#pragma once

#include "Components/ActorComponent.h"
#include "../../SPickableLoot.h"
#include "C_SPlayerPickManager.generated.h"

class ASPlayer;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_SPlayerPickManager : public UActorComponent
{
	GENERATED_BODY()

private:

	ASPlayer *player;
	ASPickableLoot *targetPickable;

	UPROPERTY(EditAnywhere, Category = "PickManager")
		float pickRange;

public:	
	UC_SPlayerPickManager();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	void OnPickInput();
};
