#pragma once

#include "Components/ActorComponent.h"
#include "C_SPlayerRecoilManager.generated.h"

class ASPlayer;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_SPlayerRecoilManager : public UActorComponent
{
	GENERATED_BODY()

private:
	ASPlayer *player;

	FVector2D currentAccumulatedRecoil;
	
	//The closer it is to 1.0f, the slower the recoil will be recovered
	//The closer it is to 0.0f, the faster the recoil will be recovered
	UPROPERTY(EditAnywhere, Category = "Recoil") float RecoilRecoveryRate;

public:

	UC_SPlayerRecoilManager();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	
	void OnShoot(const FVector2D &recoilForce, const FVector2D &recoilForceLimit);
};
