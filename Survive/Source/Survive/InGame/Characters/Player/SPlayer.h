#pragma once

#include "GameFramework/Character.h"
#include "C_SPlayerBuildManager.h"
#include "C_SPlayerMovement.h"
#include "C_SPlayerWeaponManager.h"
#include "C_SPlayerRecoilManager.h"
#include "../Enemy/SEnemy.h"
#include "SPlayer.generated.h"

class AInventoryDB;

UCLASS()
class SURVIVE_API ASPlayer : public ACharacter, public IDamageReceiver
{
	GENERATED_BODY()

private:

	UC_SPlayerWeaponManager *weaponManager;
	AInventoryDB *inventoryDB;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats) float maxHealth;

	ASPlayer();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	AActor* GetActorInFrontOfPlayer(float range = 99999.9f); //Gets the AActor in front of the player (the actor the player is aiming to)
	FVector GetShootOrigin(); //Gets the point from where the player have to shoot
	FVector GetForward(); //Gets the direction the player(camera) is facing to

	UC_SPlayerBuildManager*  GetBuildManager();
	UC_SPlayerMovement*  GetMovementManager();
	UC_SPlayerRecoilManager*  GetRecoilManager();
	UC_SPlayerWeaponManager*  GetWeaponManager();

	float GetHealthPercent() override;

	virtual void ReceiveDamage(AActor* originActor, float damage) override;
	void OnItemLooted(TSubclassOf<AInventoryItem> itemClass, int32 amount);
};
