#include "Survive.h"
#include "C_SPlayerMovement.h"
#include "SPlayer.h"


UC_SPlayerMovement::UC_SPlayerMovement()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	pitchLimits = FVector2D(-170.0f, -10.0f);
	currentPitchRotation = 0.0f;
	
	sprintSpeedMultiplier = 1.6f;

	currentBuildableWeight = currentWeaponWeight = 0.0f;
	minSpeedWeightReducer = 0.1f;

	isSprinting = false;
}


void UC_SPlayerMovement::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<ASPlayer>(GetOwner());

	player->InputComponent->BindAction("Jump", IE_Pressed, this, &UC_SPlayerMovement::OnInputSpaceBar);
	player->InputComponent->BindAction("Run", IE_Pressed, this, &UC_SPlayerMovement::OnInputRunDown);
	player->InputComponent->BindAction("Run", IE_Released, this, &UC_SPlayerMovement::OnInputRunUp);

	//Inputs ////////////////
	GetOwner()->InputComponent->BindAxis("MoveX", this, &UC_SPlayerMovement::HandleMoveX);
	GetOwner()->InputComponent->BindAxis("MoveY", this, &UC_SPlayerMovement::HandleMoveY);
	GetOwner()->InputComponent->BindAxis("TurnX", this, &UC_SPlayerMovement::HandleTurnX);
	GetOwner()->InputComponent->BindAxis("TurnY", this, &UC_SPlayerMovement::HandleTurnY);
	/////////////////////////

	originalMaxWalkSpeed = player->GetCharacterMovement()->MaxWalkSpeed;
}

void UC_SPlayerMovement::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	float speedReducer = FMath::Clamp(1.0f - (currentBuildableWeight + currentWeaponWeight), minSpeedWeightReducer, 1.0f);
	float speedIncrease = isSprinting ? sprintSpeedMultiplier : 1.0f;
	player->GetCharacterMovement()->MaxWalkSpeed = originalMaxWalkSpeed * speedReducer * speedIncrease;
}

void UC_SPlayerMovement::OnInputSpaceBar()
{
	player->Jump();
}

void UC_SPlayerMovement::OnInputRunDown()
{
	isSprinting = true;
}

void UC_SPlayerMovement::OnInputRunUp()
{
	isSprinting = false;
}

void UC_SPlayerMovement::HandleMoveX(float v)
{
	FVector mov = player->GetActorRightVector() * v;
	player->AddMovementInput(mov);
}

void UC_SPlayerMovement::HandleMoveY(float v)
{
	FVector mov = player->GetActorForwardVector() * v;
	player->AddMovementInput(mov);
}

void UC_SPlayerMovement::HandleTurnX(float v)
{
	player->AddControllerYawInput(v);
}

void UC_SPlayerMovement::HandleTurnY(float v)
{
	player->AddControllerPitchInput(v);

	FVector rot = FVector(0.0f, v, 0.0f);

	currentPitchRotation += v;
	if (currentPitchRotation >= pitchLimits.Y) currentPitchRotation = pitchLimits.Y;
	else if (currentPitchRotation <= pitchLimits.X) currentPitchRotation = pitchLimits.X;
}

