#include "Survive.h"
#include "SPlayer.h"
#include "Menuses/Inventory/InventoryDB.h"

ASPlayer::ASPlayer()
{
	PrimaryActorTick.bCanEverTick = true;

	maxHealth = 100.0f;
	health = maxHealth;
}

void ASPlayer::BeginPlay()
{
	Super::BeginPlay();

	TArray<UC_SPlayerWeaponManager*> weaponManagers; GetComponents(weaponManagers);
	weaponManager = weaponManagers[0];
}

void ASPlayer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ASPlayer::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

AActor* ASPlayer::GetActorInFrontOfPlayer(float range)
{
	TArray<AActor*> ignoredActors; ignoredActors.Add(this); //Ignore myself (the player)

	APlayerCameraManager *camManager = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	FVector traceStart = camManager->GetCameraLocation();
	FVector traceEnd = traceStart + camManager->GetCameraRotation().Vector() * range;

	FHitResult hitResult;
	if (SUtils::Trace(ignoredActors, traceStart, traceEnd, hitResult)) return hitResult.GetActor();
	else return nullptr;
}

FVector ASPlayer::GetShootOrigin()
{
	return GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraLocation();
}

FVector ASPlayer::GetForward()
{
	return GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraRotation().Vector();
}

UC_SPlayerBuildManager* ASPlayer::GetBuildManager()
{
	TArray<UC_SPlayerBuildManager*> components; GetComponents(components); 
	return components[0];
}

UC_SPlayerMovement*  ASPlayer::GetMovementManager()
{
	TArray<UC_SPlayerMovement*> components; GetComponents(components);
	return components[0];
}

UC_SPlayerRecoilManager*  ASPlayer::GetRecoilManager()
{
	TArray<UC_SPlayerRecoilManager*> components; GetComponents(components);
	return components[0];
}

UC_SPlayerWeaponManager*  ASPlayer::GetWeaponManager()
{
	TArray<UC_SPlayerWeaponManager*> components; GetComponents(components);
	return components[0];
}

float ASPlayer::GetHealthPercent() 
{ 
	return health / maxHealth;
}

void ASPlayer::OnItemLooted(TSubclassOf<AInventoryItem> itemClass, int32 amount)
{
	//Update the current weapon ammo in case the item picked is ammo
	weaponManager->OnItemLooted();
}

void ASPlayer::ReceiveDamage(AActor* originActor, float damage)
{
	health -= damage;
	if (health <= 0) this->Destroy();
}