#pragma once

#include "Components/ActorComponent.h"
#include "C_SPlayerMovement.generated.h"

class ASPlayer;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_SPlayerMovement : public UActorComponent
{
	GENERATED_BODY()

private:

	float originalMaxWalkSpeed;
	float minSpeedWeightReducer; //Lower limit for the limitation of the speed because of weight (it always should be > 0.0)

	ASPlayer *player;
	UPROPERTY(EditAnywhere, Category = "Rotation")
		FVector2D pitchLimits;

public:

	//Current weights applied by the carrying buildable and the equipped weapon
	float currentBuildableWeight, currentWeaponWeight;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rotation")
		float sprintSpeedMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rotation")
		bool isSprinting;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rotation")
		float currentPitchRotation;

	UC_SPlayerMovement();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnInputSpaceBar();
	void OnInputRunDown();
	void OnInputRunUp();

	void HandleMoveX(float v);
	void HandleMoveY(float v);
	void HandleTurnX(float v);
	void HandleTurnY(float v);
};
