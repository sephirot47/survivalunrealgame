#pragma once

#include "GameFramework/Actor.h"
#include "IDamageReceiver.generated.h"

UINTERFACE(Blueprintable, MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UDamageReceiver : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};


class IDamageReceiver
{
	GENERATED_IINTERFACE_BODY()

	virtual void ReceiveDamage(AActor* originActor, float damage);

	//Returns the health percent
	UFUNCTION(BlueprintCallable, Category = "Health")
		virtual float GetHealthPercent();
};