#include "Survive.h"
#include "C_CombatEnemy.h"

UC_CombatEnemy::UC_CombatEnemy()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	attack = 5.0f;
	attackRate = 1.0f;
	attackRange = 300.0f;

	timeLastAttack = 0.0f;
}


void UC_CombatEnemy::BeginPlay()
{
	Super::BeginPlay();

	enemy = Cast<ASEnemy>(GetOwner());
	player = Cast<ASPlayer>(GetWorld()->GetFirstPlayerController()->GetCharacter());
}


void UC_CombatEnemy::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (enemy->IsDead()) return;

	timeLastAttack += DeltaTime;
}

bool UC_CombatEnemy::CanAttack()
{
	if (enemy->IsDead()) return false;
	return timeLastAttack >= (1.0f / attackRate);
}