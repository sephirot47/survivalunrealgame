#pragma once

#include "Components/ActorComponent.h"
#include "C_CombatEnemy.h"
#include "../../Buildable/SBuildable.h"
#include "C_CombatEnemy_Ground.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_CombatEnemy_Ground : public UC_CombatEnemy
{
	GENERATED_BODY()

public:	
	UC_CombatEnemy_Ground();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	virtual void OnTickWithAValidPathToPlayer();
	virtual void OnTickWithoutAValidPathToPlayer();

	virtual void Attack(IDamageReceiver *damageReceiver) override;
};
