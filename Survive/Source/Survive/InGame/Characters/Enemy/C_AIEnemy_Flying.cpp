#include "Survive.h"
#include "C_AIEnemy_Flying.h"

UC_AIEnemy_Flying::UC_AIEnemy_Flying()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	flyingHeight = 350.0f;

	flyingWaveAmplitudeVertical = 1.0f;
	flyingWaveFrequencyVertical = 0.5f;

	flyingWaveAmplitudeHorizontal = 0.5f;
	flyingWaveFrequencyHorizontal = 0.5f;

	originalLocationWithoutWave = lastWaveMovement = FVector::ZeroVector;
	flyingWaveTimeVertical = FMath::FRand() * flyingWaveFrequencyVertical * PI;
	flyingWaveTimeHorizontal = FMath::FRand() * flyingWaveFrequencyHorizontal * PI;
}


void UC_AIEnemy_Flying::BeginPlay()
{
	Super::BeginPlay();
	enemy = Cast<ASEnemy>(GetOwner());
	TArray<UC_CombatEnemy_Flying*> combatEnemyArray;
	enemy->GetComponents(combatEnemyArray); combatEnemy = combatEnemyArray[0];

	player = Cast<ASPlayer>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	FVector loc = enemy->GetActorLocation(); loc.Z = flyingHeight;
	originalLocationWithoutWave = loc;
	enemy->SetActorLocation(originalLocationWithoutWave);
}

void UC_AIEnemy_Flying::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	//originalLocationWithoutWave = enemy->GetActorLocation() - lastWaveMovement;
	FVector dirToPlayer = player->GetActorLocation() - originalLocationWithoutWave;
	dirToPlayer.Z = 0.0f; dirToPlayer.Normalize();
	originalLocationWithoutWave += dirToPlayer * enemy->GetCharacterMovement()->MaxWalkSpeed * DeltaTime;

	FVector waveMovement = GetWaveMovement(dirToPlayer, DeltaTime); 
	enemy->SetActorLocation(originalLocationWithoutWave, true);
	enemy->SetActorLocation(originalLocationWithoutWave + waveMovement, true);
	//enemy->AddMovementInput(dirToPlayer);

	lastWaveMovement = waveMovement;
}

FVector UC_AIEnemy_Flying::GetWaveMovement(FVector &dirToPlayer, float DeltaTime)
{
	flyingWaveTimeVertical += DeltaTime;
	flyingWaveTimeHorizontal += DeltaTime;

	float insideSinVertical = flyingWaveTimeVertical * flyingWaveFrequencyVertical * PI;
	float verticalOffset = FMath::Sin(insideSinVertical) * flyingWaveAmplitudeVertical;

	float insideSinHorizontal = flyingWaveTimeHorizontal * flyingWaveFrequencyHorizontal * PI;
	float horizontalOffset = FMath::Sin(insideSinHorizontal) * flyingWaveAmplitudeHorizontal;

	FVector verticalDir = FVector::UpVector;
	FVector horizontalDir = FVector::CrossProduct(FVector::UpVector, dirToPlayer).GetSafeNormal();

	FVector verticalWaveDisplacement = verticalDir * verticalOffset;
	FVector horizontalWaveDisplacement = horizontalDir * horizontalOffset;

	return verticalWaveDisplacement + horizontalWaveDisplacement;
}