#include "Survive.h"
#include "C_CombatEnemy_Flying.h"

UC_CombatEnemy_Flying::UC_CombatEnemy_Flying()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}


void UC_CombatEnemy_Flying::BeginPlay()
{
	Super::BeginPlay();
}


void UC_CombatEnemy_Flying::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

void UC_CombatEnemy_Flying::OnTickWithAValidPathToPlayer()
{

}