#pragma once

#include "GameFramework/Character.h"
#include "../IDamageReceiver.h"
#include "../../SPickableLoot.h"
#include "../../../Menuses/Inventory/InventoryItem.h"
#include "SEnemy.generated.h"

USTRUCT(BlueprintType)
struct FSLootChance
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Stats")
		TSubclassOf<ASPickableLoot> PickableClass; //Pickable being dropped

	UPROPERTY(EditAnywhere, Category = "Stats")
		float chance; //Chance of this item being dropped when the enemy dies. (0.0 means no chance, 1.0 means always)

	UPROPERTY(EditAnywhere, Category = "Stats")
		int32 maxAmount; //Max amount of this item this enemy can drop
};

class ASPlayer;

UCLASS()
class SURVIVE_API ASEnemy : public ACharacter, public IDamageReceiver
{
	GENERATED_BODY()

private:

	ASPlayer *player;

	FTimerHandle disappearTimer;
	float disappearTime;

	bool looted;

	UPROPERTY(EditAnywhere, Category = "Loot") TArray<FSLootChance> lootItemChances;

	float dropPickableRadius; //The maximum distance from the dead enemy's body a pickable can be dropped to
	void GetRandomLootPickableLocation(FVector &location); //Returns a random position around the enemy
	void SpawnLootPickables(); //Creates all the loot pickables around the enemy 

public:

	UPROPERTY(EditAnywhere, Category = "Stats") float maxHealth;
	UPROPERTY(EditAnywhere, Category = "Stats") float health;

	ASEnemy();

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void ReceiveDamage(AActor* originActor, float damage) override;
	
	virtual void OnDieBegin();
	virtual void OnDieFinish(); //A time after the enemy died, the body disappears

	bool IsDead() { return health <= 0; }

	virtual float GetHealthPercent() override;

	UFUNCTION(BlueprintNativeEvent, Category = Damage)
		void OnReceiveDamage(AActor* originActor, float damage);
};
