#include "Survive.h"
#include "C_CombatEnemy_Ground.h"
#include "../../Buildable/SBuildable.h"


UC_CombatEnemy_Ground::UC_CombatEnemy_Ground()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	attack = 5.0f;
	attackRate = 1.0f;
	attackRange = 300.0f;

	timeLastAttack = 0.0f;
}

void UC_CombatEnemy_Ground::BeginPlay()
{
	Super::BeginPlay();
}


void UC_CombatEnemy_Ground::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UC_CombatEnemy_Ground::OnTickWithAValidPathToPlayer()
{
	if (enemy->IsDead()) return;

	TArray<ASPlayer*> players;
	SUtils::GetAll_InARadiusOf<ASPlayer>(GetWorld(), enemy, attackRange, players, true);
	for (int i = 0; i < players.Num(); ++i)
	{
		ASPlayer *player = players[i];
		Attack(player);
	}
}

void UC_CombatEnemy_Ground::OnTickWithoutAValidPathToPlayer()
{
	if (enemy->IsDead()) return;

	if (CanAttack())
	{
		ASBuildable *buildable = SUtils::GetClosestTo<ASBuildable>(GetWorld(), enemy, attackRange);
		if (buildable && buildable->GetCurrentState() != EBuildableState::BeingMoved) 
			Attack(buildable);
	}
}

void UC_CombatEnemy_Ground::Attack(IDamageReceiver *damageReceiver)
{
	if (!CanAttack()) return;

	timeLastAttack = 0.0f;
	damageReceiver->ReceiveDamage(enemy, attack);
}