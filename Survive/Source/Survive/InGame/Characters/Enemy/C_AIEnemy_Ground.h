#pragma once

#include "Components/ActorComponent.h"
#include "C_CombatEnemy_Ground.h"
#include "SEnemy.h"
#include "../Player/SPlayer.h"
#include "../../Buildable/SBuildable.h"
#include "C_AIEnemy_Ground.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_AIEnemy_Ground : public UActorComponent
{
	GENERATED_BODY()

private:
	ASEnemy *enemy;
	ASPlayer *player;
	UC_CombatEnemy_Ground *combatEnemy;

public:	
	UC_AIEnemy_Ground();

	virtual void BeginPlay() override;	
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	bool ExistsValidPathToPlayer();
};
