#include "Survive.h"
#include "SEnemy.h"
#include "../Player/SPlayer.h"

ASEnemy::ASEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	disappearTime = 0.1f;

	looted = false;

	dropPickableRadius = 100.0f;

	maxHealth = 100.0f;
	health = maxHealth;
}

void ASEnemy::BeginPlay()
{
	Super::BeginPlay();
	player = Cast<ASPlayer>(GetWorld()->GetFirstPlayerController()->GetCharacter());
}

void ASEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (IsDead()) return;
}

void ASEnemy::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void ASEnemy::ReceiveDamage(AActor* originActor, float damage)
{
	if (IsDead()) return;

	health -= damage;
	if (health <= 0) this->OnDieBegin();
	OnReceiveDamage(originActor, damage);
}

void ASEnemy::OnReceiveDamage_Implementation(AActor* originActor, float damage)
{
}

void ASEnemy::OnDieBegin()
{
	//After disappearTime seconds, Destroy the actor!
	GEngine->AddOnScreenDebugMessage(56, 1.0f, FColor::Red, TEXT("Enemy Died"));
	GetCharacterMovement()->MaxWalkSpeed = 0.0f; //Dont walk anymore :)
	GetWorld()->GetTimerManager().SetTimer(disappearTimer, this, &ASEnemy::OnDieFinish, disappearTime, false);
	SpawnLootPickables();
}

void ASEnemy::OnDieFinish()
{
	Destroy();
}

void ASEnemy::GetRandomLootPickableLocation(FVector &location)
{
	location = this->GetActorLocation();
	FRandomStream rand; rand.GenerateNewSeed();
	FVector randLocation = rand.GetUnitVector(); randLocation.Z = 0.0f;
	randLocation *= FMath::FRandRange(0.3f, 1.0f) * dropPickableRadius;

	location = this->GetActorLocation() + randLocation;
}

void ASEnemy::SpawnLootPickables()
{
	for (FSLootChance lootChanceItem : lootItemChances)
	{
		if (FMath::FRandRange(0.0f, 1.0f) <= lootChanceItem.chance) //We must drop this item
		{
			int amount = FMath::RandRange(1, lootChanceItem.maxAmount); //Calculate the amount of this object to be dropped
			
			//Create the pickable of the type this FSLootChance indicates
			FVector randLocation; GetRandomLootPickableLocation(randLocation);
			AActor *actor = GetWorld()->SpawnActor(lootChanceItem.PickableClass, &randLocation, &FRotator::ZeroRotator);
			ASPickableLoot *pickableLoot = Cast<ASPickableLoot>(actor);

			if (pickableLoot)
			{
				pickableLoot->SetAmount(amount);
			}
		}
	}
}

float ASEnemy::GetHealthPercent()
{
	return health / maxHealth;
}