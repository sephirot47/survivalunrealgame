#pragma once

#include "Components/ActorComponent.h"
#include "C_CombatEnemy.h"
#include "C_CombatEnemy_Flying.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_CombatEnemy_Flying : public UC_CombatEnemy
{
	GENERATED_BODY()

public:	

	UC_CombatEnemy_Flying();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnTickWithAValidPathToPlayer();

	//If you are flying nothing blocks u, so there's always a valid path to player
	virtual void OnTickWithoutAValidPathToPlayer() {}

	virtual void Attack(IDamageReceiver *damageReceiver) override {}
};
