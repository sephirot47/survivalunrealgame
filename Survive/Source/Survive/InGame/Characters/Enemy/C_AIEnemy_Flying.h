#pragma once

#include "Components/ActorComponent.h"
#include "C_CombatEnemy_Flying.h"
#include "SEnemy.h"
#include "../Player/SPlayer.h"
#include "../../Buildable/SBuildable.h"
#include "C_AIEnemy_Flying.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SURVIVE_API UC_AIEnemy_Flying : public UActorComponent
{
	GENERATED_BODY()

private:
	ASEnemy *enemy;
	ASPlayer *player;
	UC_CombatEnemy_Flying *combatEnemy;

	UPROPERTY(EditAnywhere, Category = "Flying Movement") float flyingHeight;

	UPROPERTY(EditAnywhere, Category = "Flying Movement") float flyingWaveAmplitudeVertical;
	UPROPERTY(EditAnywhere, Category = "Flying Movement") float flyingWaveFrequencyVertical;

	UPROPERTY(EditAnywhere, Category = "Flying Movement") float flyingWaveAmplitudeHorizontal;
	UPROPERTY(EditAnywhere, Category = "Flying Movement") float flyingWaveFrequencyHorizontal;

	float flyingWaveTimeVertical;
	float flyingWaveTimeHorizontal;

	FVector lastWaveMovement; //Last frame movement offset from originalLocationWithoutWave
	FVector originalLocationWithoutWave;

public:

	UC_AIEnemy_Flying();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	FVector GetWaveMovement(FVector &dirToPlayer, float DeltaTime);
};
