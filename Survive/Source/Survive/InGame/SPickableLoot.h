#pragma once

#include "GameFramework/Actor.h"
#include "Menuses/Inventory/InventoryDB.h"
#include "Menuses/Inventory/InventoryItem.h"
#include "SPickableLoot.generated.h"

class ASPlayer;

UCLASS()
class SURVIVE_API ASPickableLoot : public AActor
{
	GENERATED_BODY()

protected:

	ASPlayer *player; //Just a reference to the player

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickable")
		TSubclassOf<AInventoryItem> inventoryItemClass; //The bound inventory item for this pickable

	UPROPERTY(BlueprintReadOnly, Category = "Pickable")
		int32 amount; //The amount of the inventoryItem that represents this pickable

public:	

	ASPickableLoot(const class FObjectInitializer& Initializer);

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;


	TSubclassOf<AInventoryItem> GetItemClass()  { return inventoryItemClass; }

	int32 GetAmount() { return amount; }
	void SetAmount(int amount) { this->amount = amount; }

	virtual void OnTargetted();
	virtual void OnUnTargetted();
	virtual void OnPicked();

	UFUNCTION(BlueprintImplementableEvent, Category = "Pickable")
		void OnPicked_BP_Event();
	UFUNCTION(BlueprintImplementableEvent, Category = "Pickable")
		void OnTargetted_BP_Event();
	UFUNCTION(BlueprintImplementableEvent, Category = "Pickable")
		void OnUnTargetted_BP_Event();
};
