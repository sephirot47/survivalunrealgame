#include "Survive.h"
#include "SPickableLoot.h"
#include "InGame/Characters/Player/SPlayer.h"

ASPickableLoot::ASPickableLoot(const class FObjectInitializer& Initializer) : Super(Initializer)
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASPickableLoot::BeginPlay()
{
	Super::BeginPlay();
	player = Cast<ASPlayer>(GetWorld()->GetFirstPlayerController()->GetCharacter());
}

void ASPickableLoot::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ASPickableLoot::OnTargetted()
{
	OnTargetted_BP_Event();
}

void ASPickableLoot::OnUnTargetted()
{
	OnUnTargetted_BP_Event();
}

void ASPickableLoot::OnPicked()
{
	AInventoryDB::inventory->AddItem(inventoryItemClass, amount);
	player->OnItemLooted(inventoryItemClass, amount);
	OnPicked_BP_Event();
	Destroy();
}