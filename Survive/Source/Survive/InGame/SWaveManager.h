#pragma once

#include "GameFramework/Actor.h"
#include "Characters/Enemy/SEnemy.h"
#include "SWaveManager.generated.h"

USTRUCT(BlueprintType)
struct FSEnemyGroupWave
{
	GENERATED_BODY()

	//The type(class) of enemy in this group wave
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		TSubclassOf<ASEnemy> EnemyClass;

	//The amount of these enemies in this group wave
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		int32 amountEnemies;

	//Minimum level (level will be uniformly randomly picked between minLevel and maxLevel)
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		int32 minLevel;

	//Maximum level (level will be uniformly randomly picked between minLevel and maxLevel)
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		int32 maxLevel;

	//When do this groupWave starts to appear? 0.0 means at the beginning of the wave, 1.0 means at the end of the wave
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		float spawnBeginTimePercent;

	//How much does it take to spawn all the enemies(amountEnemies)? 
	//They will be uniformly spawned. For example, we are in a wave that is 100 seconds long.
	//If you have 10 enemies in this groupWave, and groupWaveDurationPercent = 0.2, this means this groupWave will take 100s*0.2 = 20s
	//and since you have 10 enemies, linearly distributed, an enemy will be spawned every 20/10=2 seconds!
	UPROPERTY(EditAnywhere, Category = "EnemyGroupWave")
		float groupWaveDurationPercent;

	int spawnedEnemies; //A variable to keep track of the enemies of this groupWave that have spawned
};

USTRUCT(BlueprintType)
struct FSWave
{
	GENERATED_BODY()

	//All the enemy wave groups in this wave
	UPROPERTY(EditAnywhere, Category = "Wave")
		TArray<FSEnemyGroupWave> enemyGroupWaves;

	//How much does the wave takes (in seconds)
	UPROPERTY(EditAnywhere, Category = "Wave")
		float waveDuration;

	//Seconds of rest after this wave :)
	UPROPERTY(EditAnywhere, Category = "Wave")
		float delayAfterWave;
};

UENUM(BlueprintType)
enum class EWaveState : uint8
{
	InWave,
	BetweenWaves
};

UCLASS()
class SURVIVE_API ASWaveManager : public AActor
{
	GENERATED_BODY()
	
private:

	int currentWaveIndex;
	FSWave currentWave;
	EWaveState currentWaveState;

	float timeSinceCurrentWaveStarted, timeSinceCurrentWaveFinished;

	void OnCurrentWaveJustStarted();
	void OnCurrentWaveJustFinished();

	FVector GetRandomSpawnLocation();

	void CheckForEnemyGroupWaveSpawning(FSEnemyGroupWave &enemyGroupWave);
	void SpawnEnemyFromEnemyGroupWave(FSEnemyGroupWave &enemyGroupWave);

	FString GetMinutesString(float timePassed);

public:	

	//An array of meshes which extents/boundaries define where an enemy can be spawned
	//The spawnVolume will be randomly chosen, as well as the spawn location inside the random selected spawnVolume
	UPROPERTY(EditAnywhere, Category = "WaveManager")
		TArray<AActor*> spawnVolumes;

	UPROPERTY(EditAnywhere, Category = "WaveManager")
		TArray<FSWave> waves;
	
	ASWaveManager();

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;


	//For the canvas wave progress bar :)
	UFUNCTION(BlueprintCallable, Category = "Wave Manager") EWaveState GetCurrentWaveState();
	UFUNCTION(BlueprintCallable, Category = "Wave Manager") float GetCurrentWaveTimePercent();
	UFUNCTION(BlueprintCallable, Category = "Wave Manager") FString GetCurrentWaveTimeText();
	UFUNCTION(BlueprintCallable, Category = "Wave Manager") float GetCurrentDelayAfterWaveTimePercent();
	UFUNCTION(BlueprintCallable, Category = "Wave Manager") FString GetCurrentDelayAfterWaveTimeText();

	FSWave GetCurrentWave() { return waves[currentWaveIndex]; }
};
