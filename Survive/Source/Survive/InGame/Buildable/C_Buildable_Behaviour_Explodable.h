#pragma once

#include "Components/ActorComponent.h"
#include "SBuildable.h"
#include "C_Buildable_Behaviour_Explodable.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_Buildable_Behaviour_Explodable : public UActorComponent, public IDestroyListener
{
	GENERATED_BODY()

private:

	bool hasExploded;

	ASBuildable *buildable;

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explodable")
		float explosionRadius;

	//Damage at the core of explosion, which linearly fades out the further the dmgReceiver is from the explosion core
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explodable")
		float maxExplosionDamage;


public:	
	UC_Buildable_Behaviour_Explodable();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Explodable")
		void OnExplode();

	void OnDestroyed(AActor* destroyedActor) override;
};
