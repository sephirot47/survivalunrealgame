#pragma once

#include "GameFramework/Actor.h"
#include "../Characters/IDamageReceiver.h"
#include "IDestroyListener.h"
#include "SBuildable.generated.h"

class AInventoryItem_Usable_Buildable;
class UC_BuildableWidget;

UENUM(BlueprintType)
enum class EBuildableState : uint8
{
	BeingMoved,		  //When the player is positioning this building
	BeingBuilt,		  //When the buildable is being built
	Built,			  //Default state
	BeingPointedOver     //When the player is pointing this building
};

UENUM(BlueprintType)
enum class EBuildableMaterial : uint8
{
	BeingMovedMaterial,
	WrongBeingMovedMaterial,
	BuiltMaterial
};

UCLASS()
class SURVIVE_API ASBuildable : public AActor, public IDamageReceiver
{
	GENERATED_BODY()
	
private:

	//Is the building hidden because of pointing nowhere when building it?
	bool hidden;
	bool finishedBuilding;

	float buildingRotationSpeed;

protected:

	UC_BuildableWidget *buildableWidgetComponent;

	EBuildableMaterial currentMaterial;
	EBuildableState currentState;

	UPROPERTY(EditAnywhere, Category = "Material") FColor colorBuildingMaterial;
	UPROPERTY(EditAnywhere, Category = "Material") FColor colorWrongBuildingMaterial;

	bool isOverlapping;

	FLinearColor GetCurrentMaterialColor();
	void ChangeMaterial(EBuildableMaterial material);
	void ApplyCurrentMaterialColors();

	//Makes it count or not when the UNavigationSystem is looking for a path
	void SetRelevantForNavigation(bool relevant);
	void SetOutline(bool outline);

public:	


	UPROPERTY(EditAnywhere, Category = "Buildable_InventoryItem")
		TSubclassOf<AInventoryItem_Usable_Buildable> CorrespondingInventoryItemClass;

	//Time it takes for the building to be built
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") float buildTime;
	float currentBuildTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") float weight;

	//The parent(reference) materials in order to create its instances
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material") UMaterial *parentTranslucentMaterialReference;

	//dynamicMaterial is set in the construction script of SBuildable
	TMap<UMeshComponent*, TArray<UMaterialInterface*>> originalMeshMaterials;
	UPROPERTY(BlueprintReadWrite, Category = "Material") UMaterialInstanceDynamic* dynamicTranslucentMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") float maxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") float health;

	ASBuildable();

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnBeingMoved();
	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnBeingBuilt();
	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnBuilt();
	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnBeingPointedOver();
	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnPointedOut();
	UFUNCTION(BlueprintNativeEvent, Category = EBuildableState) void OnDestroy();

	UFUNCTION(BlueprintCallable, Category = EBuildableState) EBuildableState GetCurrentState();

	UFUNCTION(BlueprintCallable, Category = Building) float GetBuildPercent();

	void SetCollidableWithPlayer(bool collidableWithPlayer);

	inline bool CanBeBuilt() { return !isOverlapping; }
	inline float GetBuildingRotationSpeed() { return buildingRotationSpeed; }

	virtual void ReceiveDamage(AActor* originActor, float damage) override;

	void OnOutOfPlayerBuildRange();
	void OnInsideOfPlayerBuildRange();

	void SetHidden(bool hidden) { this->hidden = hidden; SetActorHiddenInGame(hidden); }
	bool IsHidden() { return hidden; }

	UFUNCTION(BlueprintNativeEvent, Category = Damage)
		void OnReceiveDamage(AActor* originActor, float damage);

	float GetHealthPercent() override;
};
