#include "Survive.h"
#include "C_BuildableWidget.h"

UC_BuildableWidget::UC_BuildableWidget()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	widgetWidth = 512.0f;
	widgetHeight = 128.0f;
}


void UC_BuildableWidget::BeginPlay()
{
	Super::BeginPlay();

	buildable = Cast<ASBuildable>(GetOwner());
	widget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), WidgetClass);
}


void UC_BuildableWidget::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (buildable->GetCurrentState() == EBuildableState::Built || 
		buildable->GetCurrentState() == EBuildableState::BeingBuilt ||
		buildable->IsHidden())
	{
		if (widget->IsInViewport()) widget->RemoveFromViewport();
	}
	else
	{
		if (!widget->IsInViewport()) widget->AddToViewport();
		UpdateWidgetAnchors();
	}
}

void UC_BuildableWidget::UpdateWidgetAnchors()
{
	FVector2D minimum;
	GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(buildable->GetActorLocation(), minimum);
	minimum -= FVector2D(widgetWidth / 2.0f, widgetHeight / 2.0f);

	FVector2D maximum (minimum.X + widgetWidth, minimum.Y + widgetHeight);

	//Normalize the minimum and maximum to normalized device coords
	FVector2D viewportSize;
	GetWorld()->GetGameViewport()->GetViewportSize(viewportSize);
	minimum.X /= viewportSize.X; minimum.Y /= viewportSize.Y;
	maximum.X /= viewportSize.X; maximum.Y /= viewportSize.Y;
	//

	FAnchors anchors(minimum.X, minimum.Y, maximum.X, maximum.Y);
	widget->SetAnchorsInViewport(anchors);
}

void UC_BuildableWidget::OnBuildableDestroy()
{
	if (widget && widget->IsInViewport())
	{
		widget->RemoveFromViewport();
	}
}