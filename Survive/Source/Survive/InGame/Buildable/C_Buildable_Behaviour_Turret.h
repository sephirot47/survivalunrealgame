#pragma once

#include "Components/ActorComponent.h"
#include "SBuildable.h"
#include "../Characters/Enemy/SEnemy.h"
#include "C_Buildable_Behaviour_Turret.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_Buildable_Behaviour_Turret : public UActorComponent
{
	GENERATED_BODY()

protected:

	ASBuildable *buildable;

	//Particles where the hit land
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Turret")
		UParticleSystem *hitParticles;

	//Particles where the shot comes out of the barrel
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Turret")
		UParticleSystem *barrelShootParticles;

	//The name of the  USceneComponent that contains the meshes of the barrel, that will rotate when aiming :)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Turret")
		FName barrelSceneComponentName;
	USceneComponent* barrelSceneComponent;

	//The array of meshes that form the barrel. These will be rotated to aim the enemy
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Turret")
		TArray<UMeshComponent*> barrelMeshes;

	//Indicates the maximum permitted threshold of delta rotation between the rotation of the barrel and the enemy.
	//This means, the angle that forms the forward of the barrel with the enemy, must not be grater that shootRotationThresholdAngle
	//IN DEGREES
	UPROPERTY(EditAnywhere, Category = "Turret") float shootRotationThresholdAngleDegrees;

	//Indicates the maximum time the turret randomly rotates in one direction when idle
	UPROPERTY(EditAnywhere, Category = "Turret") float IdleRandomMaxRotationTime;
	UPROPERTY(EditAnywhere, Category = "Turret") float IdleRotationSpeedBoost;
	float timeSinceLastIdleRotation;
	float currentIdleMaxRotationTime;
	bool lastIdleRotationWasCW;
	bool idleRestTime;

	UPROPERTY(EditAnywhere, Category = "Turret") float damage;
	UPROPERTY(EditAnywhere, Category = "Turret") float rateOfFire;
	float timeSinceLastShot;

	UPROPERTY(EditAnywhere, Category = "Turret") float rotationSpeed;
	UPROPERTY(EditAnywhere, Category = "Turret") float turretRange;

	bool CanShoot(ASEnemy *closestEnemy);

	void HandleTurretRotation(ASEnemy *closestEnemy, float DeltaTime);
	void HandleIdleTurretRotation(float DeltaTime);

	void HandleTurretAttack(ASEnemy *closestEnemy, FHitResult &closestEnemyHitResult, float DeltaTime);


	//If the turret collides(overlaps) with something, turn back to lastRotation rotation
	//Returns true if it collided, false otherwise
	bool CheckAndFixRotationCollision(FQuat lastRotation);

public:	

	UC_Buildable_Behaviour_Turret(const FObjectInitializer& ObjectInitializer);

	virtual void Shoot(ASEnemy *closestEnemy, FHitResult &closestEnemyHitResult);

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
};
