#pragma once

#include "GameFramework/Actor.h"
#include "IDestroyListener.generated.h"

UINTERFACE(Blueprintable, MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UDestroyListener : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};


class IDestroyListener
{
	GENERATED_IINTERFACE_BODY()

	virtual void OnDestroyed(AActor* destroyedActor);
};