#include "Survive.h"
#include "IDestroyListener.h"

UDestroyListener::UDestroyListener(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void IDestroyListener::OnDestroyed(AActor* destroyedActor)
{
}
