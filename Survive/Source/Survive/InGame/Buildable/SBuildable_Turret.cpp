#include "Survive.h"
#include "SBuildable_Turret.h"

ASBuildable_Turret::ASBuildable_Turret()
{
	PrimaryActorTick.bCanEverTick = true;
	turretRange = 700.0f;
	rotationSpeed = 3.0f;
}

void ASBuildable_Turret::BeginPlay()
{
	Super::BeginPlay();
}

void ASBuildable_Turret::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

