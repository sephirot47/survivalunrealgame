#include "Survive.h"
#include "C_Buildable_Behaviour_Turret.h"


UC_Buildable_Behaviour_Turret::UC_Buildable_Behaviour_Turret(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	shootRotationThresholdAngleDegrees = 10.0f;

	damage = 5.0f;
	rateOfFire = 1.0f;
	timeSinceLastShot = 99999.9f;

	turretRange = 700.0f;
	rotationSpeed = 5.0f;

	IdleRandomMaxRotationTime = 3.0f;
	IdleRotationSpeedBoost = 3.0f;
	currentIdleMaxRotationTime = FMath::FRand() * IdleRandomMaxRotationTime + IdleRandomMaxRotationTime * 0.5f;
	timeSinceLastIdleRotation = 0.0f;
	lastIdleRotationWasCW = FMath::FRand() > 0.5f;
	idleRestTime = true;
}

void UC_Buildable_Behaviour_Turret::BeginPlay()
{
	Super::BeginPlay();
	buildable = Cast<ASBuildable>(GetOwner());

	//Get the barrelSceneComponent
	TArray<UActorComponent*> actorComps = buildable->GetComponentsByClass(USceneComponent::StaticClass());
	for (UActorComponent* actorComp : actorComps)
	{
		USceneComponent *sceneComp = Cast<USceneComponent>(actorComp);
		if (sceneComp && sceneComp->GetName().Equals(barrelSceneComponentName.ToString()))
		{
			barrelSceneComponent = sceneComp; 
			break;
		}
	}
	if (!barrelSceneComponent) barrelSceneComponent = buildable->GetRootComponent();
}


void UC_Buildable_Behaviour_Turret::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	timeSinceLastShot += DeltaTime;
	if (buildable->GetCurrentState() == EBuildableState::Built)
	{
		FHitResult closestEnemyHitResult;
		ASEnemy *closestEnemy = SUtils::GetVisibleAndClosestTo<ASEnemy>(GetWorld(), buildable, closestEnemyHitResult, turretRange, true, BuildableTurretShootTraceChannel);
		if (closestEnemy)
		{
			HandleTurretRotation(closestEnemy, DeltaTime);
			HandleTurretAttack(closestEnemy, closestEnemyHitResult, DeltaTime);
		}
		else
		{
			HandleIdleTurretRotation(DeltaTime);
		}
	}
}

void UC_Buildable_Behaviour_Turret::HandleIdleTurretRotation(float DeltaTime)
{
	timeSinceLastIdleRotation += DeltaTime;

	if (timeSinceLastIdleRotation >= currentIdleMaxRotationTime)
	{
		timeSinceLastIdleRotation = 0.0f;
		if (!idleRestTime) lastIdleRotationWasCW = !lastIdleRotationWasCW; //change cw to ccw rotation
		idleRestTime = !idleRestTime;  //change idle to rotatetime/rotatetime to idle
		currentIdleMaxRotationTime = FMath::FRand() * IdleRandomMaxRotationTime + IdleRandomMaxRotationTime * 0.5f;
	}

	if (!idleRestTime)
	{
		FRotator lastRotation = barrelSceneComponent->GetComponentRotation();
		FVector euler = FVector(0.0f, 0.0f, IdleRotationSpeedBoost * rotationSpeed * (lastIdleRotationWasCW ? -1.0f : 1.0f)) * DeltaTime;
		FQuat rotDelta = FQuat::MakeFromEuler(euler);
		barrelSceneComponent->AddWorldRotation(rotDelta);

		//If it collides, fix de rotation and try to rotate the other way :)
		CheckAndFixRotationCollision(lastRotation.Quaternion());
	}
}

void UC_Buildable_Behaviour_Turret::HandleTurretRotation(ASEnemy *closestEnemy, float DeltaTime)
{
	FRotator lastRotation = barrelSceneComponent->GetComponentRotation();

	FVector dir = (closestEnemy->GetActorLocation() - barrelSceneComponent->GetComponentLocation()).GetSafeNormal();
	FQuat destinyRotation = dir.Rotation().Quaternion();
	FQuat newRotation = FQuat::FastLerp(barrelSceneComponent->GetComponentRotation().Quaternion(), destinyRotation, DeltaTime * rotationSpeed);

	barrelSceneComponent->SetWorldRotation(newRotation.Rotator());
	CheckAndFixRotationCollision(lastRotation.Quaternion());
}

void UC_Buildable_Behaviour_Turret::HandleTurretAttack(ASEnemy *closestEnemy, FHitResult &closestEnemyHitResult, float DeltaTime)
{
	if (CanShoot(closestEnemy))
	{
		Shoot(closestEnemy, closestEnemyHitResult);
	}
}

bool UC_Buildable_Behaviour_Turret::CheckAndFixRotationCollision(FQuat lastRotation)
{
	TArray<AActor*> overlaps; buildable->GetOverlappingActors(overlaps);
	if (overlaps.Num() >= 1)
	{
		//If we rotate it and it overlaps with something, go back to the lastRotation (block the turret basically)
		barrelSceneComponent->SetWorldRotation(lastRotation);
	}
	return overlaps.Num() >= 1;
}

bool UC_Buildable_Behaviour_Turret::CanShoot(ASEnemy *closestEnemy)
{
	if (timeSinceLastShot >= 1.0f / rateOfFire)
	{
		FVector barrelDir = barrelSceneComponent->GetComponentRotation().Vector();
		FVector enemyDir = closestEnemy->GetActorLocation() - buildable->GetActorLocation();
		float dot = FVector::DotProduct(barrelDir, enemyDir);
		float degreesAngle = (1.0f - dot) * 90.0f;
		return degreesAngle <= shootRotationThresholdAngleDegrees;
	}
	return false;
}

void UC_Buildable_Behaviour_Turret::Shoot(ASEnemy *closestEnemy, FHitResult &closestEnemyHitResult)
{
	timeSinceLastShot = 0.0f;
	closestEnemy->ReceiveDamage(buildable, damage);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticles, closestEnemyHitResult.Location);
}