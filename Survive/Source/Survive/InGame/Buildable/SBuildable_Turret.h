#pragma once

#include "GameFramework/Actor.h"
#include "../../SUtils.h"
#include "../Characters/Enemy/SEnemy.h"
#include "SBuildable.h"
#include "SBuildable_Turret.generated.h"

UCLASS()
class SURVIVE_API ASBuildable_Turret : public ASBuildable
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditAnywhere, Category = "Turret")
		float rotationSpeed;

	UPROPERTY(EditAnywhere, Category = "Turret")
		float turretRange;

public:	

	ASBuildable_Turret();

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;
};
