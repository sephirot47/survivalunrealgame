#include "Survive.h"
#include "C_Buildable_Behaviour_Explodable.h"


UC_Buildable_Behaviour_Explodable::UC_Buildable_Behaviour_Explodable()
{
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
	
	hasExploded = false;
	
	maxExplosionDamage = 100.0f;
	explosionRadius = 400.0f;
}


void UC_Buildable_Behaviour_Explodable::BeginPlay()
{
	Super::BeginPlay();
	buildable = Cast<ASBuildable>(GetOwner());
}

void UC_Buildable_Behaviour_Explodable::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

}

void UC_Buildable_Behaviour_Explodable::OnDestroyed(AActor* destroyedActor)
{
	GEngine->AddOnScreenDebugMessage(123, 3.0f, FColor::Green, TEXT("asdsadsadads"));
	if (!hasExploded && buildable->health <= 0.0f)
	{
		GEngine->AddOnScreenDebugMessage(123, 3.0f, FColor::Green, TEXT("Health <= 0.0f, exploding right nao"));
		hasExploded = true;

		TArray<AActor*> affectedByExplosion;
		SUtils::GetAll_InARadiusOf<AActor>(GetWorld(), buildable, explosionRadius, affectedByExplosion);
		for (AActor *actor : affectedByExplosion)
		{
			IDamageReceiver *damageReceiver = Cast<IDamageReceiver>(actor);
			if (damageReceiver)
			{
				float dist = FVector::Dist(buildable->GetActorLocation(), actor->GetActorLocation());
				float fading = dist / explosionRadius;
				float damage = maxExplosionDamage * fading;
				damageReceiver->ReceiveDamage(buildable, damage);
			}
		}

		OnExplode();
	}
}
