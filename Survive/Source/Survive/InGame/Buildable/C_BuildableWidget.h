#pragma once

#include "Components/ActorComponent.h"
#include "SBuildable.h"
#include "C_BuildableWidget.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVE_API UC_BuildableWidget : public UActorComponent
{
	GENERATED_BODY()

private:
	ASBuildable *buildable;

	UPROPERTY(EditAnywhere, Category = "Buildable Widget") 
		float widgetWidth;
	UPROPERTY(EditAnywhere, Category = "Buildable Widget") 
		float widgetHeight;

	UPROPERTY(EditAnywhere, Category = "Buildable Widget") 
		TSubclassOf<UUserWidget> WidgetClass;

	void UpdateWidgetAnchors();

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Buildable Widget")
		UUserWidget *widget;

	UC_BuildableWidget();

	void OnBuildableDestroy();

	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
};
