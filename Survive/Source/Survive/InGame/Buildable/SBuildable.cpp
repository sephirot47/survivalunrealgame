#include "Survive.h"
#include "SBuildable.h"
#include "C_BuildableWidget.h"
#include "Menuses/Inventory/InventoryItem_Usable_Buildable.h"

ASBuildable::ASBuildable()
{
	PrimaryActorTick.bCanEverTick = true;

	currentState = EBuildableState::Built;
	
	buildTime = 5.0f;
	currentBuildTime = 0.0f;

	buildingRotationSpeed = -3.0f;

	maxHealth = 100.0f;
	health = maxHealth;
}

void ASBuildable::BeginPlay()
{
	Super::BeginPlay();

	TArray<UC_BuildableWidget*> widgetComps; GetComponents<UC_BuildableWidget>(widgetComps);
	buildableWidgetComponent = widgetComps[0];

	//Save original materials, for every mesh
	TArray<UMeshComponent*> meshes; GetComponents(meshes);
	for (UMeshComponent *mesh : meshes)
	{
		originalMeshMaterials.Add(mesh, mesh->GetMaterials());
	}

	ChangeMaterial(EBuildableMaterial::BuiltMaterial);

	if (!CorrespondingInventoryItemClass)
	{
		GEngine->AddOnScreenDebugMessage(4582, 999.9f, FColor::Red, "ERROR: A buildable class doesn't have set the CorrespondingInventoryItemClass variable \
																	 hence it will result on a buildable that cant be put back into the inventory when moving it");
	}
}

void ASBuildable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (health <= 0.0f)
	{
		OnDestroy();
	}
	else
	{
		//Check the overlaps and color it to red or green depending on it
		if (currentState == EBuildableState::BeingMoved)
		{
			TArray<AActor*> overlaps;
			GetOverlappingActors(overlaps);
			isOverlapping = overlaps.Num() >= 1;

			if (isOverlapping) ChangeMaterial(EBuildableMaterial::WrongBeingMovedMaterial);
			else ChangeMaterial(EBuildableMaterial::BeingMovedMaterial);
		}
	}

	if (currentState == EBuildableState::BeingBuilt)
	{
		currentBuildTime += DeltaTime;
		if (currentBuildTime >= buildTime)
		{
			OnBuilt();
		}
	}
	else
	{
		currentBuildTime = 0.0f;
	}

	if (currentState == EBuildableState::BeingPointedOver) SetOutline(true);
	else SetOutline(false);
}

void ASBuildable::SetRelevantForNavigation(bool relevant)
{
	TArray<UMeshComponent*> meshes; GetComponents<UMeshComponent>(meshes);
	for (UMeshComponent *mesh : meshes)
		mesh->bCanEverAffectNavigation = relevant;
}


void ASBuildable::OnBeingBuilt_Implementation()
{
	finishedBuilding = false;

	ChangeMaterial(EBuildableMaterial::BeingMovedMaterial);
	currentState = EBuildableState::BeingBuilt;
	SetCollidableWithPlayer(true);
	SetRelevantForNavigation(true);
}

void ASBuildable::OnBeingMoved_Implementation()
{
	finishedBuilding = false;

	ChangeMaterial(EBuildableMaterial::BeingMovedMaterial);
	currentState = EBuildableState::BeingMoved;
	SetCollidableWithPlayer(false);
	SetRelevantForNavigation(false);
}

void ASBuildable::OnBuilt_Implementation()
{
	finishedBuilding = true;

	ChangeMaterial(EBuildableMaterial::BuiltMaterial);
	currentState = EBuildableState::Built;
	SetCollidableWithPlayer(true);
	SetRelevantForNavigation(true);
}

void ASBuildable::OnBeingPointedOver_Implementation()
{
	ChangeMaterial(EBuildableMaterial::BuiltMaterial);
	currentState = EBuildableState::BeingPointedOver;
	SetCollidableWithPlayer(true);
	SetRelevantForNavigation(true);
}

void ASBuildable::OnPointedOut_Implementation()
{
	if (finishedBuilding) OnBuilt();
	else OnBeingBuilt();
}


void ASBuildable::OnDestroy_Implementation()
{
	buildableWidgetComponent->OnBuildableDestroy();
	TArray<UActorComponent*> actorComponents; GetComponents(actorComponents);
	for (UActorComponent* actorComponent : actorComponents)
	{
		IDestroyListener *destroyListener = Cast<IDestroyListener>(actorComponent);
		if (destroyListener) destroyListener->OnDestroyed(this);
	}
	this->Destroy();
}

EBuildableState ASBuildable::GetCurrentState()
{
	return currentState;
}

float ASBuildable::GetBuildPercent()
{
	return currentBuildTime / buildTime;
}


FLinearColor ASBuildable::GetCurrentMaterialColor()
{
	if (currentMaterial == EBuildableMaterial::BeingMovedMaterial) return FLinearColor(colorBuildingMaterial);
	return FLinearColor(colorWrongBuildingMaterial);
}

void ASBuildable::ChangeMaterial(EBuildableMaterial material)
{
	if (currentMaterial == material) return;

	currentMaterial = material;
	ApplyCurrentMaterialColors();
}

void ASBuildable::ApplyCurrentMaterialColors()
{
	bool translucent = (currentMaterial == EBuildableMaterial::WrongBeingMovedMaterial || currentMaterial == EBuildableMaterial::BeingMovedMaterial);

	if (translucent) //Set the transldynmat green or red colour
	{
		dynamicTranslucentMaterial->SetVectorParameterValue(TEXT("BaseColor"), GetCurrentMaterialColor());
	}
	else //Set the damage amount of the opaque dynamic material
	{
		//---
	}

	TArray<UMeshComponent*> meshes; GetComponents<UMeshComponent>(meshes);
	for (int i = 0; i < meshes.Num(); ++i)
	{
		for (int j = 0; j < meshes[i]->GetNumMaterials(); ++j)
		{
			meshes[i]->SetMaterial(j, translucent ? dynamicTranslucentMaterial : (*(originalMeshMaterials.Find(meshes[i])))[j]);
		}
	}
}

void ASBuildable::SetCollidableWithPlayer(bool collidableWithPlayer)
{
	TArray<UMeshComponent*> meshes;
	GetComponents<UMeshComponent>(meshes);
	for (UMeshComponent *mesh : meshes) mesh->SetCollisionResponseToChannel(ECC_Pawn, collidableWithPlayer ? ECR_Block : ECR_Overlap);
	for (UMeshComponent *mesh : meshes) mesh->SetCanEverAffectNavigation(collidableWithPlayer);
}

void ASBuildable::SetOutline(bool outline)
{
	TArray<UMeshComponent*> meshes; GetComponents(meshes);
	for (UMeshComponent* mesh : meshes) mesh->SetRenderCustomDepth(outline);
}

void ASBuildable::OnOutOfPlayerBuildRange()
{
	ChangeMaterial(EBuildableMaterial::WrongBeingMovedMaterial);
}

void ASBuildable::OnInsideOfPlayerBuildRange()
{
	if (!isOverlapping) ChangeMaterial(EBuildableMaterial::BeingMovedMaterial);
}

void ASBuildable::ReceiveDamage(AActor* originActor, float damage)
{
	if (currentState == EBuildableState::BeingMoved || currentState == EBuildableState::BeingBuilt) return;

	health -= damage;
	OnReceiveDamage(originActor, damage);
}

void ASBuildable::OnReceiveDamage_Implementation(AActor* originActor, float damage)
{
	ApplyCurrentMaterialColors();
}

float ASBuildable::GetHealthPercent()
{
	return health / maxHealth;
}