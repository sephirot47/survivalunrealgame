#pragma once

#include "InGame/Weapons/SFireWeapon_Launcher.h"
#include "InGame/Weapons/SMineLauncher_Projectile.h"
#include "InGame/Characters/Player/SPlayer.h"
#include "SFireWeapon_MineLauncher.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon_MineLauncher : public ASFireWeapon_Launcher
{
	GENERATED_BODY()

private:

	//The mine projectiles radius of action
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles") float mineRadiusOfAction;
	//The mine projectiles delay to explode, after an enemy enters on its radius of action
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles") float mineDelay;

public:

	ASFireWeapon_MineLauncher();
	virtual void LaunchProjectile() override;
};
