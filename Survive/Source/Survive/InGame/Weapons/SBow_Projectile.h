#pragma once

#include "InGame/Weapons/SFireWeapon_Projectile.h"
#include "SBow_Projectile.generated.h"

UCLASS()
class SURVIVE_API ASBow_Projectile : public ASFireWeapon_Projectile
{
	GENERATED_BODY()

private:

	FRotator lastRotation;
	float damage;

	void SetRotation(FRotator rot);

public:

	ASBow_Projectile();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void OnSticked(AActor *OtherActor, const FHitResult &hitResult) override;
	void TransferWeaponProperties(float damage);
};
