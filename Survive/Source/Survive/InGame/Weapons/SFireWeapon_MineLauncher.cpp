// Fill out your copyright notice in the Description page of Project Settings.

#include "Survive.h"
#include "SFireWeapon_MineLauncher.h"

ASFireWeapon_MineLauncher::ASFireWeapon_MineLauncher()
{
	mineDelay = 2.0f;
	mineRadiusOfAction = 200.0f;
}

void ASFireWeapon_MineLauncher::LaunchProjectile()
{
	FVector playerForward = player->GetForward();
	FVector shootOrigin = player->GetShootOrigin();
	//FVector mineLocation = GetProjectileSpawnLocation();
	FVector mineLocation = shootOrigin;
	AActor *mineActor = GetWorld()->SpawnActor(ProjectileClass, &mineLocation, &FRotator::ZeroRotator);
	ASMineLauncher_Projectile *mine = Cast<ASMineLauncher_Projectile>(mineActor);

	if (mine)
	{
		FVector aimPoint = shootOrigin + playerForward * 99999.9f;
		FVector shootDir;

		TArray<AActor*> ignore; ignore.Add(this);
		FHitResult hit;
		if (SUtils::Trace(ignore, shootOrigin, aimPoint, hit, ECollisionChannel::ECC_Visibility))
		{
			shootDir = (hit.Location - mineLocation);
			/*DrawDebugLine(GetWorld(), mineLocation, hit.Location, FColor::Blue, true,
			-1.0f, 0.0f, 1.0f);*/
		}
		else
		{
			shootDir = playerForward;
			/*DrawDebugLine(GetWorld(), mineLocation, aimPoint, FColor::Red, true,
			-1.0f, 0.0f, 1.0f);*/
		}

		mine->TransferWeaponProperties(this->damage, this->mineRadiusOfAction, this->mineDelay);
		mine->Launch(mineLocation, GetCurrentLaunchForce(), playerForward);
	}
}