#include "Survive.h"
#include "SMineLauncher_Projectile.h"

ASMineLauncher_Projectile::ASMineLauncher_Projectile()
{
	PrimaryActorTick.bCanEverTick = true;
	delayTimeCount = 0.0f;
	sticked = startedToCountDelay = false;
}

void ASMineLauncher_Projectile::BeginPlay()
{
	Super::BeginPlay();
}

void ASMineLauncher_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (sticked)
	{
		if (!startedToCountDelay) 
		{
			TArray<ASEnemy*> enemiesInRadius;
			SUtils::GetAll_InARadiusOf<ASEnemy>(GetWorld(), this, radiusOfAction, enemiesInRadius);
			startedToCountDelay = (enemiesInRadius.Num() >= 1); 
		}
		else
		{
			delayTimeCount += DeltaTime;
			if (delayTimeCount >= delay)
			{
				TArray<IDamageReceiver*> damageablesInRadius;
				SUtils::GetAll_InARadiusOf<IDamageReceiver>(GetWorld(), this, radiusOfAction, damageablesInRadius);

				//Delay time consumed, time to explode this mineee :)
				for (IDamageReceiver *dmgReceiver : damageablesInRadius)
				{
					AActor *actorReceiver = Cast<AActor>(dmgReceiver);
					if (actorReceiver)
					{
						float distanceToEnemy = FVector::Dist(actorReceiver->GetActorLocation(), this->GetActorLocation());
						float damageTakingDistanceIntoAccount = damage * (distanceToEnemy / radiusOfAction);
						dmgReceiver->ReceiveDamage(this, damageTakingDistanceIntoAccount);
					}
				}

				OnMineDestroyed(); //Trigger the event
				Destroy();
			}
		}
	}
}

void ASMineLauncher_Projectile::TransferWeaponProperties(float damage, float radiusOfAction, float delay)
{
	this->damage = damage;
	this->radiusOfAction = radiusOfAction;
	this->delay = delay;
}	

void ASMineLauncher_Projectile::OnSticked(AActor *OtherActor, const FHitResult &hitResult)
{
	//Reallocate the mine :)
	FVector origin, extents;
	this->GetActorBounds(true, origin, extents);

	FVector dirToPlayer = -launchDirection; //The direction from the hit point to the shoot point
	FVector location = hitResult.ImpactPoint;
	location += extents.Size() * dirToPlayer; //This will only work supposing the mine projectile is a sphere
	SetActorLocation(location);
	//
}
