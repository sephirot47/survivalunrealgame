#include "Survive.h"
#include "SFireWeapon_Chargeable.h"
#include "InGame/Weapons/SWeapon.h"

ASFireWeapon_Chargeable::ASFireWeapon_Chargeable()
{
	maxChargeTime = 2.0f;

	currentChargeTime = 0.0f;
	charging = false;
}

void ASFireWeapon_Chargeable::BeginPlay()
{
	Super::BeginPlay();
	player->InputComponent->BindAction("Left Mouse", IE_Released, this, &ASFireWeapon_Chargeable::OnShootRelease);
}

void ASFireWeapon_Chargeable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (charging)
	{
		//Increase charge Time
		currentChargeTime = FMath::Min(currentChargeTime + DeltaTime, maxChargeTime);
		//if (currentChargeTime >= maxChargeTime) OnShootRelease();
	}
}

void ASFireWeapon_Chargeable::Use()
{
	ASWeapon::Use();

	if (CanBeUsed()) charging = true;
	else OnNotSuccessfulWeaponUse_BP_Event();
}

//This is when the weapon really gets fired
void ASFireWeapon_Chargeable::OnShootRelease()
{
	if (charging)
	{
		charging = false;
		currentChargeTime = 0.0f;
	}
}

float ASFireWeapon_Chargeable::GetChargePercent() { return FMath::Clamp(currentChargeTime / maxChargeTime, 0.0f, 1.0f); }

