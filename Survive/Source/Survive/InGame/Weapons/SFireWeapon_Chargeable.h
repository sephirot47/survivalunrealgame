#pragma once

#include "InGame/Weapons/SFireWeapon.h"
#include "InGame/Characters/Player/SPlayer.h"
#include "SFireWeapon_Chargeable.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon_Chargeable : public ASFireWeapon
{
	GENERATED_BODY()
	
private:

	//The max time the launch can be charged
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles") float maxChargeTime;

	float currentChargeTime;
	bool charging;

protected:

	bool IsCharging() { return charging; }

public:

	ASFireWeapon_Chargeable();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void Use() override;

	//Returns the charge percent (always between 0.0f and 1.0f in case this weapon can be charged, otherwise 0.0f) 
	UFUNCTION(BlueprintCallable, Category = "Weapon Charge")
		virtual float GetChargePercent();

	//This is when the weapon really gets fired (when the player releases the charge)
	virtual void OnShootRelease();

};
