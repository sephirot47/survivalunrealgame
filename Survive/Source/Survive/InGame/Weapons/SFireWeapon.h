#pragma once

#include "InGame/Weapons/SWeapon.h"
#include "SFireWeapon.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon : public ASWeapon
{
	GENERATED_BODY()

private:

	bool lastTickWasReloading;
	float timeLastReload; //Keeps track of the time passed since the weapon started to reload

protected:

	//Recoil force, the bigger it is, the stronger the force that will push the player (In Degrees per shoot)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats") FVector2D recoilForce;

	//At how many degrees will the recoil has its cap/limit? (At both X,Y in degrees)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats") FVector2D recoilLimit;

	//Particles that spawn on the hit point
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Effects") UParticleSystem *hitParticles;

	//Particles that come out from the barrel when the weapon is used
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Effects") UParticleSystem *barrelShootParticles;

	UPROPERTY(EditAnywhere, Category = Ammo) float reloadTime; //The reload time it takes to reload this weapon

	UPROPERTY(EditAnywhere, Category = Ammo) int32 slotAmmo; //The maximum ammo for slot
	UPROPERTY(EditAnywhere, Category = Ammo) int32 totalAmmo; //The total ammo of this weapon (EXcluding the ammo loaded in the weapon(currentAmmo))
	UPROPERTY(EditAnywhere, Category = Ammo) int32 currentAmmo; //The current ammo in the slot

	//Trace in the direction the player(its camera actually) is facing, and return the array of AActors hit/overlapped
	void GetTracedActors(TArray<AActor*> &damageReceivers, TArray<FHitResult> &hitResults);

public:

	ASFireWeapon();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	virtual void Use() override;
	virtual bool CanBeUsed() override;

	virtual void Reload();

	UFUNCTION(BlueprintNativeEvent, Category = Reload) void OnReloadBegin(); 
	UFUNCTION(BlueprintNativeEvent, Category = Reload) void OnReloadFinish();

	void UpdateLocalAmmoFromInventory();
	void UpdateInventoryAmmo();

	UFUNCTION(BlueprintCallable, Category = Combat) float GetReloadTime();
	UFUNCTION(BlueprintCallable, Category = Combat) int32 GetSlotAmmo();
	UFUNCTION(BlueprintCallable, Category = Combat) int32 GetTotalAmmo();
	UFUNCTION(BlueprintCallable, Category = Combat) int32 GetCurrentAmmo();

	UFUNCTION(BlueprintCallable, Category = Combat) bool OutOfAmmo();
	UFUNCTION(BlueprintCallable, Category = Combat) bool Reloading();
};
