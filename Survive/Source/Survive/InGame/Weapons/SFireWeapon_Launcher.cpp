#include "Survive.h"
#include "SFireWeapon_Launcher.h"

ASFireWeapon_Launcher::ASFireWeapon_Launcher()
{
	minLaunchForce = 100.0f;
	maxLaunchForce = 1000.0f;
}

void ASFireWeapon_Launcher::OnShootRelease()
{
	if (IsCharging())
	{
		FHitResult hit;
		OnSuccessfulWeaponUse_BP_Event(nullptr, false, hit); //Successful, no information on which actor was hit
		NotifyWeaponUsed(); //Notify ASWeapon it has been really used (reset rate of fire time handler)
		--currentAmmo; UpdateInventoryAmmo();

		LaunchProjectile();
	}

	if (currentAmmo <= 0 && !Reloading()) Reload();

	Super::OnShootRelease(); //Yep, after :S
}


void ASFireWeapon_Launcher::LaunchProjectile()
{
}


FVector ASFireWeapon_Launcher::GetProjectileSpawnLocation()
{
	//Get the projectile spawn location
	TArray<USceneComponent*> sceneComps; GetComponents(sceneComps);
	for (USceneComponent *sc : sceneComps)
		if (sc->GetName() == projectileSpawnSceneComponentPointName)
			return sc->GetComponentLocation();
	return FVector::ZeroVector;
}

float ASFireWeapon_Launcher::GetCurrentLaunchForce()
{
	float launchForce = (maxLaunchForce - minLaunchForce) * GetChargePercent();
	return launchForce + minLaunchForce;
}