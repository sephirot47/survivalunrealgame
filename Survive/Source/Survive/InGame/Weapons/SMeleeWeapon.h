#pragma once

#include "InGame/Weapons/SWeapon.h"
#include "InGame/Characters/IDamageReceiver.h"
#include "SMeleeWeapon.generated.h"

UCLASS()
class SURVIVE_API ASMeleeWeapon : public ASWeapon
{
	GENERATED_BODY()
	

public:

	ASMeleeWeapon();

	UFUNCTION() 
		void OnOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	virtual bool BeingUsed() override;

	virtual void Use() override;
	virtual bool CanBeUsed() override;
};
