#include "Survive.h"
#include "SFireWeapon.h"
#include "../../Menuses/Inventory/InventoryDB.h"
#include "../Characters/Player/SPlayer.h"

ASFireWeapon::ASFireWeapon()
{
	timeLastReload = 999.9f;

	recoilLimit = FVector2D(2.5f, 10.0f);
	recoilForce = FVector2D(0.4f, 1.5f);

	reloadTime = 2.0f;
	slotAmmo = 10;
	totalAmmo = currentAmmo = 0;
	lastTickWasReloading = false;

	holdType = EWeaponHoldType::HoldPistol;
}

void ASFireWeapon::BeginPlay()
{
	Super::BeginPlay();
	player->InputComponent->BindAction("Weapon Reload", IE_Pressed, this, &ASFireWeapon::Reload);
	UpdateLocalAmmoFromInventory();
}

void ASFireWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	timeLastReload += DeltaTime;

	if (Reloading()) lastTickWasReloading = true;
	else if(lastTickWasReloading) 	
	{
		//He has finished reloading
		lastTickWasReloading = false;
		OnReloadFinish();
	}
}

void ASFireWeapon::GetTracedActors(TArray<AActor*> &damageReceivers, TArray<FHitResult> &hitResults)
{
	TArray<AActor*> ignoredActors;
	ignoredActors.Add(this);
	ignoredActors.Add(player);

	FVector start = player->GetShootOrigin();
	FVector end = start + player->GetForward() * 99999.9f;

	TArray<AActor*> actorsHit;
	SUtils::MultiTrace(ignoredActors, start, end, hitResults, damageReceivers, PlayerShootChannel);
}


void ASFireWeapon::Use()
{
	ASWeapon::Use();

	if (CanBeUsed()) 
	{
		TArray<AActor*> damageReceivers;
		TArray<FHitResult> hits;
		GetTracedActors(damageReceivers, hits);

		if (damageReceivers.Num() > 0)
		{
			//Successful, and hit an Actor
			AActor* actorHit = damageReceivers[0];
			FHitResult *hit = &hits[0];
			OnSuccessfulWeaponUse_BP_Event(actorHit, true, *hit);

			//If the actor is a damage receiver, damage it
			IDamageReceiver* dmgReceiver = Cast<IDamageReceiver>(actorHit);
			if (dmgReceiver) dmgReceiver->ReceiveDamage(this, damage);
		}
		else
		{
			FHitResult hit;
			OnSuccessfulWeaponUse_BP_Event(nullptr, false, hit); //Successful, but didnt hit any Actor
		}

		NotifyWeaponUsed(); //Notify ASWeapon it has been really used (reset rate of fire time handler)
		--currentAmmo; 
		UpdateInventoryAmmo();
		player->GetRecoilManager()->OnShoot(recoilForce, recoilLimit);
	}
	else //Weapon can NOT Be Used 
	{
		OnNotSuccessfulWeaponUse_BP_Event();
	}

	if (currentAmmo <= 0 && !Reloading()) Reload();
}

bool ASFireWeapon::CanBeUsed()
{
	return ASWeapon::CanBeUsed() && !Reloading() && !OutOfAmmo();
}

void ASFireWeapon::Reload()
{
	if (currentAmmo == slotAmmo || totalAmmo <= 0) return; //Doesnt make sense to reload in these case, so just dont reload
	GEngine->AddOnScreenDebugMessage(957, reloadTime, FColor::Red, TEXT("Reloading weapon..."));
	timeLastReload = 0.0f;
}

void ASFireWeapon::OnReloadBegin_Implementation() {}
void ASFireWeapon::OnReloadFinish_Implementation()
{
	int lastCurrentAmmo = currentAmmo;
	currentAmmo = FMath::Min(slotAmmo, currentAmmo + totalAmmo);
	totalAmmo -= (currentAmmo - lastCurrentAmmo);
	UpdateInventoryAmmo();
}


void ASFireWeapon::UpdateLocalAmmoFromInventory()
{
	AInventoryDB::inventory->GetAmmoForThisTypeOfWeapon(GetClass(), totalAmmo, currentAmmo);
	if (currentAmmo == -1) //If it's the first time you pick this weapon, then set the currentAmmo to the slotAmmo :D
	{
		//Load the currentAmmo as much as we can
		currentAmmo = FMath::Min(slotAmmo, totalAmmo);
		totalAmmo -= currentAmmo;
	}
}

void ASFireWeapon::UpdateInventoryAmmo()
{
	AInventoryDB::inventory->SaveAmmoForThisTypeOfWeapon(GetClass(), totalAmmo, currentAmmo);
}

float ASFireWeapon::GetReloadTime() { return reloadTime; }
int32 ASFireWeapon::GetSlotAmmo() { return slotAmmo; }
int32 ASFireWeapon::GetTotalAmmo()  { return totalAmmo; }
int32 ASFireWeapon::GetCurrentAmmo() { return currentAmmo; }


bool ASFireWeapon::OutOfAmmo() { return currentAmmo <= 0; }
bool ASFireWeapon::Reloading() { return timeLastReload < reloadTime; }