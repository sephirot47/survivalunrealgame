#include "Survive.h"
#include "SFireWeapon_Bow.h"

ASFireWeapon_Bow::ASFireWeapon_Bow()
{
}

void ASFireWeapon_Bow::LaunchProjectile()
{
	FVector playerForward = player->GetForward();
	FVector shootOrigin = player->GetShootOrigin();
	//FVector arrowLocation = GetProjectileSpawnLocation();
	FVector arrowLocation = shootOrigin;
	FRotator arrowRot = playerForward.Rotation();
	AActor *arrowActor = GetWorld()->SpawnActor(ProjectileClass, &arrowLocation, &arrowRot);
	ASBow_Projectile *arrow = Cast<ASBow_Projectile>(arrowActor);

	if (arrow)
	{
		FVector aimPoint = shootOrigin + playerForward * 99999.9f;
		FVector shootDir; 

		TArray<AActor*> ignore; ignore.Add(this);
		FHitResult hit;
		if (SUtils::Trace(ignore, shootOrigin, aimPoint, hit, ECollisionChannel::ECC_Visibility))
		{
			shootDir = (hit.Location - arrowLocation);
			/*DrawDebugLine(GetWorld(), arrowLocation, hit.Location, FColor::Blue, true,
				-1.0f, 0.0f, 1.0f);*/
		}
		else
		{
			shootDir = playerForward;
			/*DrawDebugLine(GetWorld(), arrowLocation, aimPoint, FColor::Red, true,
				-1.0f, 0.0f, 1.0f);*/
		}

		arrow->TransferWeaponProperties(this->damage);
		arrow->Launch(arrowLocation, GetCurrentLaunchForce(), shootDir.GetSafeNormal()); //Launch the arrow
	}
}