
#include "Survive.h"
#include "SFireWeapon_Projectile.h"
#include "InGame/Characters/Player/SPlayer.h"


ASFireWeapon_Projectile::ASFireWeapon_Projectile()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASFireWeapon_Projectile::BeginPlay()
{
	Super::BeginPlay();

	//Add the begin overlap delegate on the colliding sphere
	TArray<USphereComponent*> sphereColls; GetComponents<USphereComponent>(sphereColls);
	if (sphereColls.Num() >= 1)
	{
		collidingSphere = sphereColls[0];
		collidingSphere->OnComponentBeginOverlap.AddDynamic(this, &ASFireWeapon_Projectile::OnOverlapSomething);
	}
	else GEngine->AddOnScreenDebugMessage(34564, 99.9f, FColor::Red, TEXT("ERROR: No SphereCollider in this projectile!"));

	lastProjLocation = FVector::ZeroVector;
	currentProjLocation = FVector::ZeroVector;
}

void ASFireWeapon_Projectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	lastProjLocation = currentProjLocation;
	currentProjLocation = collidingSphere->GetComponentLocation();

	if (sticked)
	{
		if (!GetAttachParentActor())
		{
			//If its parent (the actor where the projectile attached) doesn't exist anymore, destroy the projectile
			Destroy();
		}
	}
}


void ASFireWeapon_Projectile::Launch(const FVector &projectileInitialLocation, float launchForce, const FVector &launchDirection)
{
	SetActorLocation(projectileInitialLocation);
	SetActorRotation(launchDirection.Rotation());

	this->launchDirection = launchDirection;

	/*DrawDebugLine(GetWorld(), projectileInitialLocation, projectileInitialLocation + launchDirection * 999999.9f, FColor::Green, true,
		-1.0f, 0.0f, 3.0f);*/

	TArray<UMeshComponent*> meshes; GetComponents<UMeshComponent>(meshes);
	for (UMeshComponent *mesh : meshes)
	{
		mesh->AddForce(launchDirection.GetSafeNormal() * launchForce);
	}
}

void ASFireWeapon_Projectile::OnOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (!sticked)
	{
		FCollisionQueryParams traceParams(FName(TEXT("VictoreCore Trace")), true);
		traceParams.bTraceComplex = true; traceParams.bReturnPhysicalMaterial = false;
		TArray<AActor*> ignore; ignore.Add(this); ignore.Add(GetWorld()->GetFirstPlayerController()->GetCharacter());
		traceParams.AddIgnoredActors(ignore);
		FHitResult hitComplex = FHitResult(ForceInit);

		FVector start = lastProjLocation;
		FVector end = start + (currentProjLocation - start).GetSafeNormal() * 999999.9f;
		//GEngine->AddOnScreenDebugMessage(12, 3.0f, FColor::Red, start.ToString());
		//GEngine->AddOnScreenDebugMessage(13, 3.0f, FColor::Green, end.ToString());
		//DrawDebugLine(GetWorld(), start, end, FColor::Green, true, 999.9f, 0, 2.0f);
		if (GetWorld()->LineTraceSingleByChannel(hitComplex, start, end, PlayerShootChannel, traceParams))
		{
			sticked = true; //Sticked to the hit actor
			SetCollidable(false);
			StopLaunching();
			OnSticked(&(*hitComplex.Actor), hitComplex);

			TArray<USceneComponent*> scenes; GetComponents<USceneComponent>(scenes);
			for (USceneComponent *scene : scenes)
				scene->AttachTo(&(*hitComplex.Component), hitComplex.BoneName, EAttachLocation::KeepWorldPosition, true);
			this->AttachRootComponentTo(&(*hitComplex.Component), hitComplex.BoneName, EAttachLocation::KeepWorldPosition, true);

			//GEngine->AddOnScreenDebugMessage(1244, 3.0f, FColor::Cyan, TEXT("Sticked to " + hitComplex.Actor->GetName()));
			//GEngine->AddOnScreenDebugMessage(1245, 3.0f, FColor::Cyan, TEXT("Sticked to component " + hitComplex.Component->GetName()));
			//GEngine->AddOnScreenDebugMessage(1246, 3.0f, FColor::Cyan, TEXT("Sticked to bone " + hitComplex.BoneName.ToString()));
		}
	}
}

void ASFireWeapon_Projectile::StopLaunching()
{
	TArray<UMeshComponent*> meshes; GetComponents<UMeshComponent>(meshes);
	for (UMeshComponent *mesh : meshes) mesh->SetSimulatePhysics(false);
}

void ASFireWeapon_Projectile::SetCollidable(bool collidable)
{
	this->SetActorEnableCollision(collidable);

	TArray<UPrimitiveComponent*> primitiveComponents; GetComponents<UPrimitiveComponent>(primitiveComponents);
	for (UPrimitiveComponent *primitiveComp : primitiveComponents)
	{
		if (!collidable) primitiveComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		primitiveComp->SetCollisionEnabled(collidable ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision);
	}
}

FVector ASFireWeapon_Projectile::GetPhysicsVelocity()
{
	TArray<UMeshComponent*> meshes; GetComponents(meshes);
	return meshes.Num() <= 0 ? FVector::ZeroVector : meshes[0]->GetPhysicsLinearVelocity();
}