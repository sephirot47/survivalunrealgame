#pragma once

#include "InGame/Weapons/SFireWeapon_Launcher.h"
#include "InGame/Weapons/SBow_Projectile.h"
#include "InGame/Characters/Player/SPlayer.h"
#include "SFireWeapon_Bow.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon_Bow : public ASFireWeapon_Launcher
{
	GENERATED_BODY()

private:

public:
	ASFireWeapon_Bow();
	virtual void LaunchProjectile() override;
};
