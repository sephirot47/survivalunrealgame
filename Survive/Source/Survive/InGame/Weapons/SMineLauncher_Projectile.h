// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InGame/Weapons/SFireWeapon_Projectile.h"
#include "InGame/Characters/Enemy/SEnemy.h"
#include "SMineLauncher_Projectile.generated.h"

UCLASS()
class SURVIVE_API ASMineLauncher_Projectile : public ASFireWeapon_Projectile
{
	GENERATED_BODY()
	
private:

	float radiusOfAction; //Given by the weapon too
	float delay; //Given by the weapon too

	float delayTimeCount; //To keep track of the delay time since the mine activated

	bool startedToCountDelay;

public:	

	ASMineLauncher_Projectile();

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	void TransferWeaponProperties(float damage, float radiusOfAction, float delay);

	virtual void OnSticked(AActor *OtherActor, const FHitResult &hitResult) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Mine Projectile Events")
		void OnMineDestroyed();
};
