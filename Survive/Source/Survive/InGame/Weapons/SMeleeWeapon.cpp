#include "Survive.h"
#include "SMeleeWeapon.h"

ASMeleeWeapon::ASMeleeWeapon()
{
	holdType = EWeaponHoldType::HoldMelee;
}

void ASMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();

	TArray<UBoxComponent*> boxColliders; GetComponents<UBoxComponent>(boxColliders);
	for (UBoxComponent *boxCollider : boxColliders)
	{
		//GEngine->AddOnScreenDebugMessage(1244, 3.0f, FColor::Cyan, boxCollider->GetName());
		boxCollider->OnComponentBeginOverlap.AddDynamic(this, &ASMeleeWeapon::OnOverlapSomething);
	}
}

void ASMeleeWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASMeleeWeapon::Use()
{
	ASWeapon::Use();
	if (CanBeUsed())
	{
		FHitResult hit;
		OnSuccessfulWeaponUse_BP_Event(nullptr, false, hit);
	}
	else OnNotSuccessfulWeaponUse_BP_Event();
}

void ASMeleeWeapon::OnOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	//GEngine->AddOnScreenDebugMessage(1243, 3.0f, FColor::Green, TEXT("Overlap with " + OtherActor->GetName()));
	if (OtherActor != this && BeingUsed())
	{
		//GEngine->AddOnScreenDebugMessage(1244, 3.0f, FColor::Green, TEXT("Sticked to " + OtherActor->GetName()));
		//GEngine->AddOnScreenDebugMessage(1245, 3.0f, FColor::Cyan, TEXT("Sticked to component " + OtherComp->GetName()));
		//GEngine->AddOnScreenDebugMessage(1246, 3.0f, FColor::Red, TEXT("Sticked to bone " + SweepResult.BoneName.ToString()));

		IDamageReceiver *damageReceiver = Cast<IDamageReceiver>(OtherActor);
		if (damageReceiver)
		{
			//GEngine->AddOnScreenDebugMessage(1247, 3.0f, FColor::Green, TEXT("Receive damagee"));
			damageReceiver->ReceiveDamage(OtherActor, this->damage);
		}
	}
}

bool ASMeleeWeapon::BeingUsed()  { return usingWeapon; }

bool ASMeleeWeapon::CanBeUsed()
{
	return true;
}
