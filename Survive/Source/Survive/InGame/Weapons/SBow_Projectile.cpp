#include "Survive.h"
#include "SBow_Projectile.h"



ASBow_Projectile::ASBow_Projectile()
{

}

void ASBow_Projectile::BeginPlay()
{
	Super::BeginPlay();
	SetActorHiddenInGame(true); //To avoid a frame of wrong rotation at the very beginning
}

void ASBow_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Face the direction of your velocity
	if (!sticked)
	{
		if (GetPhysicsVelocity() != FVector::ZeroVector) this->SetRotation(GetPhysicsVelocity().Rotation());
		else this->SetRotation(launchDirection.Rotation());

		lastRotation = GetActorRotation();
	}
	//
	SetActorHiddenInGame(false);
}

void ASBow_Projectile::OnSticked(AActor *OtherActor, const FHitResult &hitResult)
{
	SetActorLocation(hitResult.ImpactPoint - collidingSphere->GetComponentLocation());
	
	//Fix rotation using the last rotation the arrow had before sticking (the physx engine)
	//may distort the arrow rotation when penetrating OtherActor
	SetActorRotation(hitResult.ImpactNormal.Rotation().Quaternion());

	IDamageReceiver *damageReceiver = Cast<IDamageReceiver>(OtherActor);
	if (damageReceiver)
	{
		damageReceiver->ReceiveDamage(this, damage);
	}
}

void ASBow_Projectile::TransferWeaponProperties(float damage)
{
	this->damage = damage;
}

void ASBow_Projectile::SetRotation(FRotator rot)
{
	TArray<UMeshComponent*> meshes; GetComponents(meshes);
	for (UMeshComponent *mesh : meshes)
	{
		mesh->SetRelativeRotation(rot);
	}
}