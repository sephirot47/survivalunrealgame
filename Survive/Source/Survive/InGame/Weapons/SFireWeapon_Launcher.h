#pragma once

#include "InGame/Weapons/SFireWeapon_Chargeable.h"
#include "InGame/Weapons/SFireWeapon_Projectile.h"
#include "SFireWeapon_Launcher.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon_Launcher : public ASFireWeapon_Chargeable
{
	GENERATED_BODY()

private:

	//The min launch force
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles") float minLaunchForce;
	//The max launch force
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles") float maxLaunchForce;

	//The name of the sceneComponent indicating the point where the projectiles spawn
	UPROPERTY(EditAnywhere, Category = "Weapon Projectiles")
		FString projectileSpawnSceneComponentPointName;

protected:

	FVector GetProjectileSpawnLocation();

public:

	ASFireWeapon_Launcher();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Projectiles")
		TSubclassOf<ASFireWeapon_Projectile> ProjectileClass;

	virtual void LaunchProjectile();

	virtual void OnShootRelease() override;
	float GetCurrentLaunchForce();
};
