#pragma once

#include "GameFramework/Actor.h"
#include "SFireWeapon_Projectile.generated.h"

UCLASS()
class SURVIVE_API ASFireWeapon_Projectile : public AActor
{
	GENERATED_BODY()
	
private:
	FVector currentProjLocation, lastProjLocation;

protected:

	USphereComponent *collidingSphere;

	FVector launchDirection;
	float damage; //Given by the weapon

	bool sticked; //Has the projectile been sticked to something?

	void SetCollidable(bool collidable);
	void StopLaunching();
	FVector GetPhysicsVelocity();

public:	
	ASFireWeapon_Projectile();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION() 
		void OnOverlapSomething(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	void Launch(const FVector &weaponLocation, float launchForce, const FVector &launchDirection);

	virtual void OnSticked(AActor *stickedToThisActor, const FHitResult &hitResult) {}
};
